﻿using Bogus;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Seeders
{
    public class ChildCareFamilyProfileSeeder : ISeeder<ChildCareFamilyProfile>
    {
        public List<ChildCareFamilyProfile> Generate(int maxAmount)
        {
            List<ChildCareFamilyProfile> childCareFamilyProfiles = new Faker<ChildCareFamilyProfile>()
                //The Id needs to be increased by 1 because entities in MS SQL Server are non zero indexed.
                .RuleFor(profile => profile.Id, faker => faker.IndexFaker + 1)
                .RuleFor(profile => profile.UserId, faker => faker.System.AndroidId())
                .RuleFor(profile => profile.Title, faker => faker.Lorem.Sentence(3))
                .RuleFor(profile => profile.Place, faker => faker.Address.ZipCode())
                .RuleFor(profile => profile.City, faker => faker.Address.City())
                .RuleFor(profile => profile.Description, faker => faker.Lorem.Paragraph())
                .RuleFor(profile => profile.Price, faker => faker.Finance.Amount(5, 150))
                .RuleFor(profile => profile.ChildrenAmount, faker => faker.Random.Int(1, 7))
                .RuleFor(profile => profile.CareSubType, faker => (CareSubType)faker.Random.Int(0, 2))
                .RuleFor(profile => profile.AgeGroup, faker => (AgeGroup)faker.Random.Int(0, 4))
                //All Id's of one-to-one relations need to be increased by maxAmount + 1. So they don't collide with personal profile entities.
                .RuleFor(profile => profile.AdditionalServicesId, faker => faker.IndexFaker + maxAmount + 1)
                .RuleFor(profile => profile.AdditionalTraitsId, faker => faker.IndexFaker + maxAmount + 1)
                .RuleFor(profile => profile.AvailabilityId, faker => faker.IndexFaker + maxAmount + 1)
                .Generate(maxAmount);

            return childCareFamilyProfiles;
        }
    }
}
