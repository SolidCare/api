﻿using Bogus;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Data.Seeders
{
    public class AdditionalServicesSeeder : ISeeder<AdditionalServices>
    {
        public List<AdditionalServices> Generate(int maxAmount)
        {
            List<AdditionalServices> additionalServicesList = new Faker<AdditionalServices>()
                .RuleFor(additionalServices => additionalServices.Id, faker => faker.IndexFaker + 1)
                .RuleFor(additionalServices => additionalServices.Cook, faker => faker.Random.Bool())
                .RuleFor(additionalServices => additionalServices.EscortChild, faker => faker.Random.Bool())
                .RuleFor(additionalServices => additionalServices.HelpWithHomework, faker => faker.Random.Bool())
                .RuleFor(additionalServices => additionalServices.HelpWithHousekeeping, faker => faker.Random.Bool())
                .RuleFor(additionalServices => additionalServices.MedicationDispensing, faker => faker.Random.Bool())
                //Twice the amount because both family and personal profile reference this with an one-to-one relationship.
                .Generate(maxAmount*2);
            return additionalServicesList;
        }
    }
}
