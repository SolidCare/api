﻿using Bogus;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Seeders
{
    public class ChildCarePersonalProfileSeeder : ISeeder<ChildCarePersonalProfile>
    {
        public List<ChildCarePersonalProfile> Generate(int maxAmount)
        {
            List<ChildCarePersonalProfile> childCarePersonalProfiles = new Faker<ChildCarePersonalProfile>()
                // The Id and Id's of the one-to-one relation need to be increased by 1 because entities in MS SQL Server are non zero indexed.
                .RuleFor(profile => profile.Id, faker => faker.IndexFaker + 1)
                .RuleFor(profile => profile.UserId, faker => faker.System.AndroidId())
                .RuleFor(profile => profile.Title, faker => faker.Lorem.Sentence(3))
                .RuleFor(profile => profile.Place, faker => faker.Address.ZipCode())
                .RuleFor(profile => profile.City, faker => faker.Address.City())
                .RuleFor(profile => profile.Description, faker => faker.Lorem.Paragraph())
                .RuleFor(profile => profile.Price, faker => faker.Finance.Amount(5, 150))
                .RuleFor(profile => profile.MaxChildren, faker => faker.Random.Int(1, 7))
                .RuleFor(profile => profile.Experience, faker => faker.Random.Int(0, 30))
                .RuleFor(profile => profile.CareSubType, faker => (CareSubType)faker.Random.Int(0, 2))
                .RuleFor(profile => profile.AgeGroup, faker => (AgeGroup)faker.Random.Int(0, 4))
                .RuleFor(profile => profile.AdditionalServicesId, faker => faker.IndexFaker + 1)
                .RuleFor(profile => profile.AdditionalTraitsId, faker => faker.IndexFaker + 1)
                .RuleFor(profile => profile.AvailabilityId, faker => faker.IndexFaker + 1)
                .Generate(maxAmount);

            return childCarePersonalProfiles;
        }
    }
}
