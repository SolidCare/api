﻿using Bogus;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Seeders
{
    public class AvailabilitySeeder : ISeeder<Availability>
    {
        public List<Availability> Generate(int maxAmount)
        {
            List<Availability> availabilityList = new Faker<Availability>()
                    .RuleFor(availability => availability.Id, faker => faker.IndexFaker + 1)
                    .RuleFor(availability => availability.AvailableFrom, faker => faker.Date.Soon())
                    .RuleFor(availability => availability.Frequency, faker => (Frequency)faker.Random.Int(0,2))
                    .RuleFor(availability => availability.Workinghours, faker => faker.Random.Int(0,12))
                    .RuleFor(availability => availability.WorkinghoursFrequency, faker => (WorkinghoursFrequency)faker.Random.Int(0, 2))

                    .RuleFor(availability => availability.MondayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.MondayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.MondayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.MondayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.TuesdayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.TuesdayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.TuesdayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.TuesdayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.WednesdayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.WednesdayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.WednesdayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.WednesdayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.ThursdayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.ThursdayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.ThursdayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.ThursdayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.FridayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.FridayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.FridayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.FridayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.SaturdayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SaturdayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SaturdayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SaturdayNight, faker => faker.Random.Bool(0.25f))

                    .RuleFor(availability => availability.SundayMorning, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SundayAfternoon, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SundayEvening, faker => faker.Random.Bool(0.25f))
                    .RuleFor(availability => availability.SundayNight, faker => faker.Random.Bool(0.25f))

                    //Twice the amount because both family and personal profile reference this with an one-to-one relationship.
                    .Generate(maxAmount*2);

            return availabilityList;
        }
    }
}
