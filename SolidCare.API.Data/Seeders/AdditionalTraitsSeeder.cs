﻿using Bogus;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Data.Seeders
{
    public class AdditionalTraitsSeeder : ISeeder<AdditionalTraits>
    {
        public List<AdditionalTraits> Generate(int maxAmount)
        {
            List<AdditionalTraits> additionalTraitsList = new Faker<AdditionalTraits>()
                .RuleFor(additionalTraits => additionalTraits.Id, faker => faker.IndexFaker + 1)
                .RuleFor(additionalTraits => additionalTraits.AcceptsPets, faker => faker.Random.Bool())
                .RuleFor(additionalTraits => additionalTraits.CarLicense, faker => faker.Random.Bool())
                .RuleFor(additionalTraits => additionalTraits.FirstAid, faker => faker.Random.Bool())
                .RuleFor(additionalTraits => additionalTraits.Smoker, faker => faker.Random.Bool())
                //Twice the amount because both family and personal profile reference this with an one-to-one relationship.
                .Generate(maxAmount*2);
            return additionalTraitsList;
        }
    }
}
