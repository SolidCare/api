﻿namespace SolidCare.API.Data.Models.Enums
{
    /// <summary>
    /// Contains age stages of a childs growing phases.
    /// Used to define an approximate age of the children of the family.
    /// </summary>
    public enum AgeGroup
    {
        /**
         * Newborn 0-1 Year
         * Toddler 1-3 Year
         * Preschool 3-5 Year
         * Gradeschooler 5-12 Year
         * Teen 12-18 Year
         */
        Newborn, 
        Toddler, 
        Preschool, 
        Gradeschooler, 
        Teen
    }
}
