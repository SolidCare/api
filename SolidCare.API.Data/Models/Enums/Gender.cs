﻿namespace SolidCare.API.Data.Models.Enums
{
    /// <summary>
    /// Contains possible selectable genders.
    /// </summary>
    public enum Gender
    {
        Male, 
        Female, 
        Diverse
    }
}
