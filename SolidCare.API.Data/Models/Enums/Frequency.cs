﻿namespace SolidCare.API.Data.Models.Enums
{
    /// <summary>
    /// Contains frequency of the availability.
    /// Used to give a better understanding about how often the care personal is available.
    /// </summary>
    public enum Frequency
    {
        Regular, 
        Once, 
        Occasional
    }
}
