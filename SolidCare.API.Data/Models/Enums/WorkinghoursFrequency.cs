﻿namespace SolidCare.API.Data.Models.Enums
{
    /// <summary>
    /// Contains the working frequency of the availability.
    /// Used to give a better understanding about how regularly the care personal is available.
    /// </summary>
    public enum WorkinghoursFrequency
    {
        Daily, 
        Weekly, 
        Monthly
    }
}
