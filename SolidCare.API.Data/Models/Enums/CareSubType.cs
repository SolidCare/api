﻿namespace SolidCare.API.Data.Models.Enums
{
    /// <summary>
    /// Contains specific types of child care personal.
    /// Used to give a better understanding what kind of child care is offered.
    /// </summary>
    public enum CareSubType
    {
        Babysitter, 
        Nanny, 
        DayCareMother
    }
}
