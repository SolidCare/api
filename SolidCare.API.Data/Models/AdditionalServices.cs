﻿using SolidCare.API.Data.Contracts;

namespace SolidCare.API.Data.Models
{
    /// <summary>
    /// Care personal can define additional service they might offer.
    /// Additional Services are one-to-one related with ChildCarePersonalProfile.
    /// </summary>
    public class AdditionalServices : IUserObject
    {
        public int Id { get; set; }
        public bool Cook { get; set; }
        public bool HelpWithHomework { get; set; }
        public bool HelpWithHousekeeping { get; set; }
        public bool EscortChild { get; set; }
        public bool MedicationDispensing { get; set; }
        public string UserId { get; set; }
    }
}
