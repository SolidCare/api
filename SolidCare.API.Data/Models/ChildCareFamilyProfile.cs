﻿using System.ComponentModel.DataAnnotations;
using SolidCare.API.Data.Contracts;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Models
{
    /// <summary>
    /// Displays available child care family profiles.
    /// Includes three entities: AdditionalServices, AdditionalTraits, Availability
    /// All included entities are one-to-one related.
    /// </summary>
    public class ChildCareFamilyProfile : IUserObject
    {
        public int Id { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Place { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int ChildrenAmount { get; set; }

        public CareSubType CareSubType { get; set; }
        public AgeGroup AgeGroup { get; set; }

        [Required]
        public int AdditionalServicesId { get; set; }
        public AdditionalServices AdditionalServices { get; set; }

        [Required]
        public int AdditionalTraitsId { get; set; }
        public AdditionalTraits AdditionalTraits { get; set; }

        [Required]
        public int AvailabilityId { get; set; }
        public Availability Availability { get; set; }
    }
}
