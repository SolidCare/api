﻿namespace SolidCare.API.Data.Models
{
    /// <summary>
    /// Care personal can define additional traits that might impact their qualities.
    /// Additional traits are one-to-one related with ChildCarePersonalProfile.
    /// </summary>
    public class AdditionalTraits
    {
        public int Id { get; set; }
        public bool Smoker { get; set; }
        public bool AcceptsPets { get; set; }
        public bool CarLicense { get; set; }
        public bool FirstAid { get; set; }
    }
}
