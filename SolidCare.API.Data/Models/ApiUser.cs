﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Models
{
    /// <summary>
    /// User Model.
    /// Extends IdentityUser from Identity Core.
    /// </summary>
    public class ApiUser : IdentityUser
    {
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public Gender Gender { get; set; }

    }
}
