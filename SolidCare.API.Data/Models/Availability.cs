﻿using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Data.Models
{
    /// <summary>
    /// Care personal define times from and when they are available.
    /// Availabilities are one-to-one related with ChildCarePersonalProfile.
    /// </summary>
    public class Availability
    {
        public int Id { get; set; }

        public DateTime AvailableFrom { get; set; }

        public Frequency Frequency { get; set; }

        //The amount of time the care personal will work at a time.
        public int Workinghours { get; set; }

        public WorkinghoursFrequency WorkinghoursFrequency { get; set; }

        /// <summary>
        /// Week table
        /// Displays an overview when the care personal is available thou out the week and time of the day.
        /// These are separated properties to store them in the Db.
        /// </summary>
        public bool MondayMorning { get; set; }
        public bool MondayAfternoon { get; set; }
        public bool MondayEvening { get; set; }
        public bool MondayNight { get; set; }
        public bool TuesdayMorning { get; set; }
        public bool TuesdayAfternoon { get; set; }
        public bool TuesdayEvening { get; set; }
        public bool TuesdayNight { get; set; }
        public bool WednesdayMorning { get; set; }
        public bool WednesdayAfternoon { get; set; }
        public bool WednesdayEvening { get; set; }
        public bool WednesdayNight { get; set; }
        public bool ThursdayMorning { get; set; }
        public bool ThursdayAfternoon { get; set; }
        public bool ThursdayEvening { get; set; }
        public bool ThursdayNight { get; set; }
        public bool FridayMorning { get; set; }
        public bool FridayAfternoon { get; set; }
        public bool FridayEvening { get; set; }
        public bool FridayNight { get; set; }
        public bool SaturdayMorning { get; set; }
        public bool SaturdayAfternoon { get; set; }
        public bool SaturdayEvening { get; set; }
        public bool SaturdayNight { get; set; }
        public bool SundayMorning { get; set; }
        public bool SundayAfternoon { get; set; }
        public bool SundayEvening { get; set; }
        public bool SundayNight { get; set; }

    }
}
