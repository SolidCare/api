﻿namespace SolidCare.API.Data.Contracts
{
    /// <summary>
    /// Contract to add the UserId to Models.
    /// </summary>
    public interface IUserObject
    {
        public string UserId { get; set; }
    }
}
