﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Data.Configurations;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;

namespace SolidCare.API.Data
{
    public class ApiDbContext : IdentityDbContext<ApiUser>
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options){}

        //DbSet inits
        public DbSet<AdditionalServices> AdditionalServices { get; set; }
        public DbSet<AdditionalTraits> AdditionalTraits { get; set; }
        public DbSet<Availability> Availability { get; set; }
        public DbSet<ChildCarePersonalProfile> ChildCarePersonalProfile { get; set; }
        public DbSet<ChildCareFamilyProfile> ChildCareFamilyProfile { get; set; }

        /// <summary>
        /// Runs seeder methods.
        /// </summary>
        /// <param name="modelBuilder">Base param</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Amount of entities to be created by seeders.
            const int entityAmount = 5;

            //Configurations
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            

            //Seeders
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AdditionalServices>().HasData(new AdditionalServicesSeeder().Generate(entityAmount));
            modelBuilder.Entity<AdditionalTraits>().HasData(new AdditionalTraitsSeeder().Generate(entityAmount));
            modelBuilder.Entity<Availability>().HasData(new AvailabilitySeeder().Generate(entityAmount));
            modelBuilder.Entity<ChildCarePersonalProfile>().HasData(new ChildCarePersonalProfileSeeder().Generate(entityAmount));
            modelBuilder.Entity<ChildCareFamilyProfile>().HasData(new ChildCareFamilyProfileSeeder().Generate(entityAmount));
        }
    }
}
