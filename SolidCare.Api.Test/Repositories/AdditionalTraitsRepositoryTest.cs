﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Paging;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Repositories
{
    public class AdditionalTraitsRepositoryTest
    {
        private ApiDbContext context;
        private AdditionalTraitsRepository repository;
        private List<AdditionalTraits> mockList;

        /// <summary>
        /// Sets up a in memory database.
        /// With this it is possible to setup a fresh database for every new test.
        /// </summary>
        /// <param name="memoryDbName">CallerMemberName gets the name of the test and sets it as the database name. Ensures a new in memory Db for every test.</param>
        /// <returns>Repository with database context.</returns>
        private async Task<AdditionalTraitsRepository> ArrangeMockDb([CallerMemberName] string memoryDbName = "")
        {
            const int maxAmount = 5;
            mockList = new AdditionalTraitsSeeder().Generate(maxAmount);

            var options = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase(databaseName: memoryDbName)
                .Options;

            context = new ApiDbContext(options);

            context.AdditionalTraits.AddRange(mockList);

            await context.SaveChangesAsync();

            return new AdditionalTraitsRepository(context);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            repository = await ArrangeMockDb();

            var additionalTraitsList = await repository.GetAllAsync();

            Assert.NotNull(additionalTraitsList);
            Assert.Equal(mockList.Count, additionalTraitsList.Count);
            Assert.IsAssignableFrom<List<AdditionalTraits>>(additionalTraitsList);
        }

        [Fact]
        public async Task Add_NoParameters_AddsEntity()
        {
            repository = await ArrangeMockDb();

            AdditionalTraits newAdditionalTraits = new()
            {
                AcceptsPets = true,
                CarLicense = true,
                FirstAid = true,
                Smoker = true
            };

            var additionalTraitsList = await repository.AddAsync(newAdditionalTraits);

            Assert.NotNull(additionalTraitsList);
            Assert.Equal(newAdditionalTraits, additionalTraitsList);
            Assert.IsAssignableFrom<AdditionalTraits>(additionalTraitsList);
        }

        [Fact]
        public async Task GetAllPaged_NoParameters_ReturnsEntitiesOfPage()
        {
            repository = await ArrangeMockDb();
            QueryParameters queryParameters = new()
            {
                PageNumber = 1,
                PageSize = 2,
                StartIndex = 9
            };

            var additionalTraitsPagedList = await repository.GetAllPagedAsync(queryParameters);

            Assert.NotNull(additionalTraitsPagedList);
            Assert.Single(additionalTraitsPagedList);
            Assert.IsAssignableFrom<List<AdditionalTraits>>(additionalTraitsPagedList);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            repository = await ArrangeMockDb();

            var additionalTraits = await repository.GetAsync(mockList[0].Id);

            Assert.NotNull(additionalTraits);
            Assert.Equal(mockList[0], additionalTraits);
            Assert.IsAssignableFrom<AdditionalTraits>(additionalTraits);
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            int listLengthBefore = await repository.Count();

            await repository.DeleteAsync(testId);

            int listLengthAfter = await repository.Count();

            Assert.False(await repository.Exists(testId));
            Assert.Equal(listLengthBefore - 1, listLengthAfter);
        }

        [Fact]
        public async Task Update_NoParameters_UpdatesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;
            const bool expectedProperty = true;

            var updateAdditionalTraits = repository.GetAsync(testId).Result;

            updateAdditionalTraits.AcceptsPets = expectedProperty;
            updateAdditionalTraits.CarLicense = expectedProperty;
            updateAdditionalTraits.FirstAid = expectedProperty;
            updateAdditionalTraits.Smoker = expectedProperty;
            
            await repository.UpdateAsync(updateAdditionalTraits);

            Assert.True(await repository.Exists(updateAdditionalTraits.Id));

            Assert.True(repository.GetAsync(testId).Result.AcceptsPets);
            Assert.True(repository.GetAsync(testId).Result.CarLicense);
            Assert.True(repository.GetAsync(testId).Result.FirstAid);
            Assert.True(repository.GetAsync(testId).Result.Smoker);
        }

        [Fact]
        public async Task Count_NoParameters_ReturnsEntitiesCount()
        {
            repository = await ArrangeMockDb();

            var additionalTraitsCount = await repository.Count();

            Assert.Equal(mockList.Count, additionalTraitsCount);
            Assert.IsAssignableFrom<int>(additionalTraitsCount);
        }

        [Fact]
        public async Task Exists_NoParameters_ReturnsTrueIfEntityExists()
        {
            repository = await ArrangeMockDb();

            var additionalTraitsExists = await repository.Exists(mockList[0].Id);

            Assert.True(additionalTraitsExists);

            additionalTraitsExists = await repository.Exists(20);

            Assert.False(additionalTraitsExists);
        }
    }
}
