﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Paging;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Repositories
{
    public class ChildCarePersonalProfileRepositoryTest
    {
        private ApiDbContext context;
        private ChildCarePersonalProfileRepository repository;
        private List<ChildCarePersonalProfile> mockList;

        /// <summary>
        /// Sets up a in memory database.
        /// With this it is possible to setup a fresh database for every new test.
        /// </summary>
        /// <param name="memoryDbName">CallerMemberName gets the name of the test and sets it as the database name. Ensures a new in memory Db for every test.</param>
        /// <returns>Repository with database context.</returns>
        private async Task<ChildCarePersonalProfileRepository> ArrangeMockDb([CallerMemberName] string memoryDbName = "")
        {
            const int maxAmount = 5;
            mockList = new ChildCarePersonalProfileSeeder().Generate(maxAmount);

            var options = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase(databaseName: memoryDbName)
                .Options;

            context = new ApiDbContext(options);

            context.ChildCarePersonalProfile.AddRange(mockList);

            await context.SaveChangesAsync();

            return new ChildCarePersonalProfileRepository(context);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            repository = await ArrangeMockDb();

            var childCarePersonalProfileList = await repository.GetAllAsync();

            Assert.NotNull(childCarePersonalProfileList);
            Assert.Equal(mockList.Count, childCarePersonalProfileList.Count);
            Assert.IsAssignableFrom<List<ChildCarePersonalProfile>>(childCarePersonalProfileList);
        }

        [Fact]
        public async Task Add_NoParameters_AddsEntity()
        {
            repository = await ArrangeMockDb();

            ChildCarePersonalProfile newChildCarePersonalProfile = new()
            {
                AgeGroup = AgeGroup.Newborn,
                CareSubType =CareSubType.DayCareMother,
                MaxChildren = 3,
                Experience = 15,
                City = "TestCity",
                Description = "TestDescription",
                Place = "8117",
                Price = 30.5m,
                Title = "TestTitle",
                UserId = "TestUserId"
            };

            var childCarePersonalProfileList = await repository.AddAsync(newChildCarePersonalProfile);

            Assert.NotNull(childCarePersonalProfileList);
            Assert.Equal(newChildCarePersonalProfile, childCarePersonalProfileList);
            Assert.IsAssignableFrom<ChildCarePersonalProfile>(childCarePersonalProfileList);
        }

        [Fact]
        public async Task GetAllPaged_NoParameters_ReturnsEntitiesOfPage()
        {
            repository = await ArrangeMockDb();
            QueryParameters queryParameters = new()
            {
                PageNumber = 1,
                PageSize = 2,
                StartIndex = 4
            };

            var childCarePersonalProfilePagedList = await repository.GetAllPagedAsync(queryParameters);

            Assert.NotNull(childCarePersonalProfilePagedList);
            Assert.Single(childCarePersonalProfilePagedList);
            Assert.IsAssignableFrom<List<ChildCarePersonalProfile>>(childCarePersonalProfilePagedList);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            repository = await ArrangeMockDb();

            var childCarePersonalProfile = await repository.GetAsync(mockList[0].Id);

            Assert.NotNull(childCarePersonalProfile);
            Assert.Equal(mockList[0], childCarePersonalProfile);
            Assert.IsAssignableFrom<ChildCarePersonalProfile>(childCarePersonalProfile);
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            int listLengthBefore = await repository.Count();

            await repository.DeleteAsync(testId);

            int listLengthAfter = await repository.Count();

            Assert.False(await repository.Exists(testId));
            Assert.Equal(listLengthBefore - 1, listLengthAfter);
        }

        [Fact]
        public async Task Update_NoParameters_UpdatesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            //Seeder maximum Price is 150.
            const decimal expectedPrice = 35.5m;

            var updateChildCarePersonalProfile = repository.GetAsync(testId).Result;
            Assert.NotEqual(expectedPrice, updateChildCarePersonalProfile.Price);

            updateChildCarePersonalProfile.Price = expectedPrice;

            await repository.UpdateAsync(updateChildCarePersonalProfile);

            Assert.True(await repository.Exists(updateChildCarePersonalProfile.Id));
            Assert.Equal(expectedPrice, repository.GetAsync(testId).Result.Price);
        }

        [Fact]
        public async Task Count_NoParameters_ReturnsEntitiesCount()
        {
            repository = await ArrangeMockDb();

            var childCarePersonalProfileCount = await repository.Count();

            Assert.Equal(mockList.Count, childCarePersonalProfileCount);
            Assert.IsAssignableFrom<int>(childCarePersonalProfileCount);
        }

        [Fact]
        public async Task Exists_NoParameters_ReturnsTrueIfEntityExists()
        {
            repository = await ArrangeMockDb();

            var childCarePersonalProfileExists = await repository.Exists(mockList[0].Id);

            Assert.True(childCarePersonalProfileExists);

            childCarePersonalProfileExists = await repository.Exists(20);

            Assert.False(childCarePersonalProfileExists);
        }
    }
}
