﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Paging;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Repositories
{
    public class ChildCareFamilyProfileRepositoryTest
    {
        private ApiDbContext context;
        private ChildCareFamilyProfileRepository repository;
        private List<ChildCareFamilyProfile> mockList;

        /// <summary>
        /// Sets up a in memory database.
        /// With this it is possible to setup a fresh database for every new test.
        /// </summary>
        /// <param name="memoryDbName">CallerMemberName gets the name of the test and sets it as the database name. Ensures a new in memory Db for every test.</param>
        /// <returns>Repository with database context.</returns>
        private async Task<ChildCareFamilyProfileRepository> ArrangeMockDb([CallerMemberName] string memoryDbName = "")
        {
            const int maxAmount = 5;
            mockList = new ChildCareFamilyProfileSeeder().Generate(maxAmount);

            var options = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase(databaseName: memoryDbName)
                .Options;

            context = new ApiDbContext(options);

            context.ChildCareFamilyProfile.AddRange(mockList);

            await context.SaveChangesAsync();

            return new ChildCareFamilyProfileRepository(context);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            repository = await ArrangeMockDb();

            var childCareFamilyProfileList = await repository.GetAllAsync();

            Assert.NotNull(childCareFamilyProfileList);
            Assert.Equal(mockList.Count, childCareFamilyProfileList.Count);
            Assert.IsAssignableFrom<List<ChildCareFamilyProfile>>(childCareFamilyProfileList);
        }

        [Fact]
        public async Task Add_NoParameters_AddsEntity()
        {
            repository = await ArrangeMockDb();

            ChildCareFamilyProfile newChildCareFamilyProfile = new()
            {
                AgeGroup = AgeGroup.Newborn,
                CareSubType =CareSubType.DayCareMother,
                ChildrenAmount = 3,
                City = "TestCity",
                Description = "TestDescription",
                Place = "8117",
                Price = 30.5m,
                Title = "TestTitle",
                UserId = "TestUserId"
            };

            var childCareFamilyProfileList = await repository.AddAsync(newChildCareFamilyProfile);

            Assert.NotNull(childCareFamilyProfileList);
            Assert.Equal(newChildCareFamilyProfile, childCareFamilyProfileList);
            Assert.IsAssignableFrom<ChildCareFamilyProfile>(childCareFamilyProfileList);
        }

        [Fact]
        public async Task GetAllPaged_NoParameters_ReturnsEntitiesOfPage()
        {
            repository = await ArrangeMockDb();
            QueryParameters queryParameters = new()
            {
                PageNumber = 1,
                PageSize = 2,
                StartIndex = 4
            };

            var childCareFamilyProfilePagedList = await repository.GetAllPagedAsync(queryParameters);

            Assert.NotNull(childCareFamilyProfilePagedList);
            Assert.Single(childCareFamilyProfilePagedList);
            Assert.IsAssignableFrom<List<ChildCareFamilyProfile>>(childCareFamilyProfilePagedList);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            repository = await ArrangeMockDb();

            var childCareFamilyProfile = await repository.GetAsync(mockList[0].Id);

            Assert.NotNull(childCareFamilyProfile);
            Assert.Equal(mockList[0], childCareFamilyProfile);
            Assert.IsAssignableFrom<ChildCareFamilyProfile>(childCareFamilyProfile);
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            int listLengthBefore = await repository.Count();

            await repository.DeleteAsync(testId);

            int listLengthAfter = await repository.Count();

            Assert.False(await repository.Exists(testId));
            Assert.Equal(listLengthBefore - 1, listLengthAfter);
        }

        [Fact]
        public async Task Update_NoParameters_UpdatesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            //Seeder maximum Price is 150.
            const decimal expectedPrice = 200.5m;

            var updateChildCareFamilyProfile = repository.GetAsync(testId).Result;
            Assert.NotEqual(expectedPrice, updateChildCareFamilyProfile.Price);

            updateChildCareFamilyProfile.Price = expectedPrice;

            await repository.UpdateAsync(updateChildCareFamilyProfile);

            Assert.True(await repository.Exists(updateChildCareFamilyProfile.Id));
            Assert.Equal(expectedPrice, repository.GetAsync(testId).Result.Price);
        }

        [Fact]
        public async Task Count_NoParameters_ReturnsEntitiesCount()
        {
            repository = await ArrangeMockDb();

            var childCareFamilyProfileCount = await repository.Count();

            Assert.Equal(mockList.Count, childCareFamilyProfileCount);
            Assert.IsAssignableFrom<int>(childCareFamilyProfileCount);
        }

        [Fact]
        public async Task Exists_NoParameters_ReturnsTrueIfEntityExists()
        {
            repository = await ArrangeMockDb();

            var childCareFamilyProfileExists = await repository.Exists(mockList[0].Id);

            Assert.True(childCareFamilyProfileExists);

            childCareFamilyProfileExists = await repository.Exists(20);

            Assert.False(childCareFamilyProfileExists);
        }
    }
}
