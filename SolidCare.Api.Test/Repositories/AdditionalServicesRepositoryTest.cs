﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Paging;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Repositories
{
    public class AdditionalServicesRepositoryTest
    {
        private ApiDbContext context;
        private AdditionalServicesRepository repository;
        private List<AdditionalServices> mockList;

        /// <summary>
        /// Sets up a in memory database.
        /// With this it is possible to setup a fresh database for every new test.
        /// </summary>
        /// <param name="memoryDbName">CallerMemberName gets the name of the test and sets it as the database name. Ensures a new in memory Db for every test.</param>
        /// <returns>Repository with database context.</returns>
        private async Task<AdditionalServicesRepository> ArrangeMockDb([CallerMemberName]string memoryDbName = "")
        {
            const int maxAmount = 5;
            mockList = new AdditionalServicesSeeder().Generate(maxAmount);

            var options = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase(databaseName: memoryDbName)
                .Options;

            context = new ApiDbContext(options);

            context.AdditionalServices.AddRange(mockList);

            await context.SaveChangesAsync();

            return new AdditionalServicesRepository(context);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            repository = await ArrangeMockDb();

            var additionalServicesList = await repository.GetAllAsync();

            Assert.NotNull(additionalServicesList);
            Assert.Equal(mockList.Count, additionalServicesList.Count);
            Assert.IsAssignableFrom<List<AdditionalServices>>(additionalServicesList);
        }

        [Fact]
        public async Task Add_NoParameters_AddsEntity()
        {
            repository = await ArrangeMockDb();

            AdditionalServices newAdditionalServices = new AdditionalServices()
            {
                Cook = true,
                EscortChild = true,
                HelpWithHomework = true,
                HelpWithHousekeeping = true,
                MedicationDispensing = true
            };

            var additionalServicesList = await repository.AddAsync(newAdditionalServices);

            Assert.NotNull(additionalServicesList);
            Assert.Equal(newAdditionalServices, additionalServicesList);
            Assert.IsAssignableFrom<AdditionalServices>(additionalServicesList);
        }

        [Fact]
        public async Task GetAllPaged_NoParameters_ReturnsEntitiesOfPage()
        {
            repository = await ArrangeMockDb();
            QueryParameters queryParameters = new()
            {
                PageNumber = 1,
                PageSize = 2,
                StartIndex = 9
            };

            var additionalServicesPagedList = await repository.GetAllPagedAsync(queryParameters);

            Assert.NotNull(additionalServicesPagedList);
            Assert.Single(additionalServicesPagedList);
            Assert.IsAssignableFrom<List<AdditionalServices>>(additionalServicesPagedList);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            repository = await ArrangeMockDb();

            var additionalServices = await repository.GetAsync(mockList[0].Id);

            Assert.NotNull(additionalServices);
            Assert.Equal(mockList[0], additionalServices);
            Assert.IsAssignableFrom<AdditionalServices>(additionalServices);
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            int listLengthBefore = await repository.Count();

            await repository.DeleteAsync(testId);

            int listLengthAfter = await repository.Count();

            Assert.False(await repository.Exists(testId));
            Assert.Equal(listLengthBefore -1, listLengthAfter);
        }

        [Fact]
        public async Task Update_NoParameters_UpdatesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;
            const string expectedUserId = "TestUserId";

            var updateAdditionalServices = repository.GetAsync(testId).Result;
            Assert.NotEqual(expectedUserId, updateAdditionalServices.UserId);

            updateAdditionalServices.UserId = expectedUserId;
            
            await repository.UpdateAsync(updateAdditionalServices);

            Assert.True(await repository.Exists(updateAdditionalServices.Id));
            Assert.Equal(expectedUserId, repository.GetAsync(testId).Result.UserId);
        }

        [Fact]
        public async Task Count_NoParameters_ReturnsEntitiesCount()
        {
            repository = await ArrangeMockDb();
            
            var additionalServicesCount = await repository.Count();
            
            Assert.Equal(mockList.Count, additionalServicesCount);
            Assert.IsAssignableFrom<int>(additionalServicesCount);
        }

        [Fact]
        public async Task Exists_NoParameters_ReturnsTrueIfEntityExists()
        {
            repository = await ArrangeMockDb();

            var additionalServicesExists = await repository.Exists(mockList[0].Id);

            Assert.True(additionalServicesExists);

            additionalServicesExists = await repository.Exists(20);

            Assert.False(additionalServicesExists);
        }
    }
}
