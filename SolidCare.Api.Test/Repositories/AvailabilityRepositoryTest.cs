﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Paging;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Models.Enums;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Repositories
{
    public class AvailabilityRepositoryTest
    {
        private ApiDbContext context;
        private AvailabilityRepository repository;
        private List<Availability> mockList;

        /// <summary>
        /// Sets up a in memory database.
        /// With this it is possible to setup a fresh database for every new test.
        /// </summary>
        /// <param name="memoryDbName">CallerMemberName gets the name of the test and sets it as the database name. Ensures a new in memory Db for every test.</param>
        /// <returns>Repository with database context.</returns>
        private async Task<AvailabilityRepository> ArrangeMockDb([CallerMemberName] string memoryDbName = "")
        {
            const int maxAmount = 5;
            mockList = new AvailabilitySeeder().Generate(maxAmount);

            var options = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase(databaseName: memoryDbName)
                .Options;

            context = new ApiDbContext(options);

            context.Availability.AddRange(mockList);

            await context.SaveChangesAsync();

            return new AvailabilityRepository(context);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            repository = await ArrangeMockDb();

            var availabilityList = await repository.GetAllAsync();

            Assert.NotNull(availabilityList);
            Assert.Equal(mockList.Count, availabilityList.Count);
            Assert.IsAssignableFrom<List<Availability>>(availabilityList);
        }

        [Fact]
        public async Task Add_NoParameters_AddsEntity()
        {
            repository = await ArrangeMockDb();

            Availability newAvailability = new()
            {
                AvailableFrom = new DateTime(2022, 04, 01),
                Frequency = Frequency.Once,
                Workinghours = 4,
                WorkinghoursFrequency = WorkinghoursFrequency.Monthly,

                MondayMorning = true,
                MondayAfternoon = true,
                MondayEvening = true,
                MondayNight = true,

                TuesdayMorning = true,
                TuesdayAfternoon = true,
                TuesdayEvening = true,
                TuesdayNight = true,

                WednesdayMorning = true,
                WednesdayAfternoon = true,
                WednesdayEvening = true,
                WednesdayNight = true,

                ThursdayMorning = true,
                ThursdayAfternoon = true,
                ThursdayEvening = true,
                ThursdayNight = true,

                FridayMorning = true,
                FridayAfternoon = true,
                FridayEvening = true,
                FridayNight = true,

                SaturdayMorning = true,
                SaturdayAfternoon = true,
                SaturdayEvening = true,
                SaturdayNight = true,

                SundayMorning = true,
                SundayAfternoon = true,
                SundayEvening = true,
                SundayNight = true,
            };

            var availabilityList = await repository.AddAsync(newAvailability);

            Assert.NotNull(availabilityList);
            Assert.Equal(newAvailability, availabilityList);
            Assert.IsAssignableFrom<Availability>(availabilityList);
        }

        [Fact]
        public async Task GetAllPaged_NoParameters_ReturnsEntitiesOfPage()
        {
            repository = await ArrangeMockDb();
            QueryParameters queryParameters = new()
            {
                PageNumber = 1,
                PageSize = 2,
                StartIndex = 9
            };

            var availabilityPagedList = await repository.GetAllPagedAsync(queryParameters);

            Assert.NotNull(availabilityPagedList);
            Assert.Single(availabilityPagedList);
            Assert.IsAssignableFrom<List<Availability>>(availabilityPagedList);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            repository = await ArrangeMockDb();

            var availability = await repository.GetAsync(mockList[0].Id);

            Assert.NotNull(availability);
            Assert.Equal(mockList[0], availability);
            Assert.IsAssignableFrom<Availability>(availability);
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            int listLengthBefore = await repository.Count();

            await repository.DeleteAsync(testId);

            int listLengthAfter = await repository.Count();

            Assert.False(await repository.Exists(testId));
            Assert.Equal(listLengthBefore - 1, listLengthAfter);
        }

        [Fact]
        public async Task Update_NoParameters_UpdatesEntity()
        {
            repository = await ArrangeMockDb();

            int testId = mockList[0].Id;

            //Seeders maximum WorkingHours is 12.
            const int expectedWorkingHours = 30;

            var updateAvailability = repository.GetAsync(testId).Result;
            Assert.NotEqual(expectedWorkingHours, updateAvailability.Workinghours);
            updateAvailability.Workinghours = expectedWorkingHours;

            await repository.UpdateAsync(updateAvailability);

            Assert.True(await repository.Exists(updateAvailability.Id));
            Assert.Equal(expectedWorkingHours, repository.GetAsync(testId).Result.Workinghours);
        }

        [Fact]
        public async Task Count_NoParameters_ReturnsEntitiesCount()
        {
            repository = await ArrangeMockDb();

            var availabilityCount = await repository.Count();

            Assert.Equal(mockList.Count, availabilityCount);
            Assert.IsAssignableFrom<int>(availabilityCount);
        }

        [Fact]
        public async Task Exists_NoParameters_ReturnsTrueIfEntityExists()
        {
            repository = await ArrangeMockDb();

            var availabilityExists = await repository.Exists(mockList[0].Id);

            Assert.True(availabilityExists);

            availabilityExists = await repository.Exists(20);

            Assert.False(availabilityExists);
        }
    }
}
