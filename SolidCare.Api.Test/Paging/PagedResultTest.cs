﻿using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Paging
{
    public class PagedResultTest
    {
        [Fact]
        public void CalcMaxPageNum_NoParameters_ReturnsNumberOfPages()
        {
            const int pageSize = 3;
            const int pageNumber = 1;
            var mockList = new AdditionalServicesSeeder().Generate(10);

            var pagedResult = new PagedResult<AdditionalServices>(
                mockList.Count, 
                pageSize, 
                pageNumber, 
                mockList
            );

            const int expected = 7;
            var actual = pagedResult.CalcMaxPageNum(mockList.Count, pageSize);
            
            Assert.Equal(expected, actual);
        }
    }
}
