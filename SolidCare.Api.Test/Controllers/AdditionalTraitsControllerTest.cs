﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using SolidCare.API.Controllers;
using SolidCare.API.Core.Configurations;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.AdditionalTraits;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Controllers
{
    public class AdditionalTraitsControllerTest
    {
        private readonly Mock<IAdditionalTraitsRepository> repository = new();
        private readonly Mock<IAuthenticationService> authService = new();
        private readonly IConfigurationProvider configProvider = new MapperConfiguration(config => config.AddProfile(new AutoMapperConfig()));
        private readonly AdditionalTraitsController controller;

        public AdditionalTraitsControllerTest()
        {
            IMapper mapper = new Mapper(configProvider);
            controller = new AdditionalTraitsController(repository.Object, authService.Object, mapper);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            const int maxAmount = 5;
            var mockList = new AdditionalTraitsSeeder().Generate(maxAmount);

            repository.Setup(x => x.GetAllAsync()).ReturnsAsync(mockList);

            var additionalTraitsList = await controller.GetAll();

            Assert.NotNull(additionalTraitsList);
            Assert.Equal(maxAmount * 2, additionalTraitsList.Count());
            Assert.IsAssignableFrom<IEnumerable<GetAdditionalTraitsDto>>(additionalTraitsList);
        }

        [Fact]
        public async Task GetPaged_NoParameters_ReturnsEntitiesOfSelectedPage()
        {
            const int maxAmount = 5;
            var mockList = new AdditionalTraitsSeeder().Generate(maxAmount);


            QueryParameters queryParameters = new QueryParameters { PageNumber = 1, PageSize = 2, StartIndex = 9 };

            repository.Setup(x => x.GetAllPagedAsync(queryParameters)).ReturnsAsync(new List<AdditionalTraits>() { mockList[9] });

            var additionalTraitsList = await controller.GetPaged(queryParameters);

            Assert.NotNull(additionalTraitsList.Value);
            Assert.Single(additionalTraitsList.Value.Items);
            Assert.IsAssignableFrom<IEnumerable<GetAdditionalTraitsDto>>(additionalTraitsList.Value.Items);
            Assert.IsAssignableFrom<PagedResult<GetAdditionalTraitsDto>>(additionalTraitsList.Value);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            const int maxAmount = 1;
            var mockItem = new AdditionalTraitsSeeder().Generate(maxAmount)[0];

            repository.Setup(x => x.GetAsync(mockItem.Id)).ReturnsAsync(mockItem);

            var additionalTraitsList = await controller.Get(mockItem.Id);

            Assert.NotNull(additionalTraitsList.Value);
            Assert.Equal(mockItem.Id, additionalTraitsList.Value.Id);
            Assert.IsAssignableFrom<GetAdditionalTraitsDto>(additionalTraitsList.Value);
        }
    }
}