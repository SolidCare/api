﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SolidCare.API.Controllers;
using SolidCare.API.Core.Configurations;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.ChildCareFamilyProfile;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Controllers
{
    public class ChildCareFamilyProfileControllerTest
    {
        private readonly Mock<IChildCareFamilyProfileRepository> repository = new();
        private readonly Mock<IAdditionalServicesRepository> additionalServicesRepository = new();
        private readonly Mock<IAdditionalTraitsRepository> additionalTraitsRepository = new();
        private readonly Mock<IAvailabilityRepository> availabilityRepository = new();
        private readonly Mock<IAuthenticationService> authService = new();
        private readonly IConfigurationProvider configProvider = new MapperConfiguration(config => config.AddProfile(new AutoMapperConfig()));

        private readonly ChildCareFamilyProfileController controller;

        private readonly IMapper mapper;

        public ChildCareFamilyProfileControllerTest()
        {
            mapper = new Mapper(configProvider);
            controller = new ChildCareFamilyProfileController(
                repository.Object,
                additionalServicesRepository.Object,
                additionalTraitsRepository.Object,
                availabilityRepository.Object,
                authService.Object,
                mapper);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            const int maxAmount = 5;
            var mockList = new ChildCareFamilyProfileSeeder().Generate(maxAmount);

            repository.Setup(x => x.GetAllAsync()).ReturnsAsync(mockList);

            var childCareFamilyProfileList = await controller.GetAll();

            Assert.NotNull(childCareFamilyProfileList);
            Assert.Equal(maxAmount, childCareFamilyProfileList.Count());
            Assert.IsAssignableFrom<IEnumerable<GetAllChildCareFamilyProfileDto>>(childCareFamilyProfileList);
        }

        [Fact]
        public async Task GetPaged_NoParameters_ReturnsEntitiesOfSelectedPage()
        {
            const int maxAmount = 5;
            var mockList = new ChildCareFamilyProfileSeeder().Generate(maxAmount);

            QueryParameters queryParameters = new QueryParameters { PageNumber = 1, PageSize = 2, StartIndex = 4 };

            repository.Setup(x => x.GetAllPagedAsync(queryParameters)).ReturnsAsync(new List<ChildCareFamilyProfile>{ mockList[4] });

            var childCareFamilyProfileList = await controller.GetPaged(queryParameters);

            Assert.NotNull(childCareFamilyProfileList.Value);
            Assert.Single(childCareFamilyProfileList.Value.Items);
            Assert.IsAssignableFrom<IEnumerable<GetAllChildCareFamilyProfileDto>>(childCareFamilyProfileList.Value.Items);
            Assert.IsAssignableFrom<PagedResult<GetAllChildCareFamilyProfileDto>>(childCareFamilyProfileList.Value);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            const int maxAmount = 1;
            var mockItem = new ChildCareFamilyProfileSeeder().Generate(maxAmount)[0];

            repository.Setup(x => x.GetDetails(mockItem.Id)).ReturnsAsync(mockItem);

            var childCareFamilyProfileList = await controller.Get(mockItem.Id);

            Assert.NotNull(childCareFamilyProfileList.Value);
            Assert.Equal(mockItem.Id, childCareFamilyProfileList.Value.Id);
            Assert.IsAssignableFrom<GetChildCareFamilyProfileDto>(childCareFamilyProfileList.Value);
        }

        [Fact]
        public async Task Post_NoParameters_AddsEntity()
        {
            const int maxAmount = 1;
            var mockItem = new ChildCareFamilyProfileSeeder().Generate(maxAmount)[0];

            repository.Setup(x => x.AddAsync(It.IsAny<ChildCareFamilyProfile>())).ReturnsAsync(mockItem);

            var createChildCareFamilyProfile = mapper.Map<CreateChildCareFamilyProfileDto>(mockItem);

            await controller.PostChildCareFamilyProfile(createChildCareFamilyProfile);

            repository.Verify(x => x.AddAsync(It.IsAny<ChildCareFamilyProfile>()), Times.Exactly(1));
        }

        [Fact]
        public async Task Delete_NoParameters_DeletesEntity()
        {
            const int maxAmount = 1;
            var mockItem = new ChildCareFamilyProfileSeeder().Generate(maxAmount)[0];
            mockItem.UserId = "TestUserId";

            repository.Setup(x => x.GetAsync(mockItem.Id)).ReturnsAsync(mockItem);
            repository.Setup(x => x.DeleteAsync(mockItem.Id));
            authService.Setup(x => x.GetUserId(It.IsAny<ControllerBase>())).Returns("TestUserId");

            await controller.Delete(mockItem.Id);

            repository.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Exactly(1));
        }

        [Fact]
        public async Task Put_NoParameters_UpdatesEntity()
        {
            const int expectedPrice = 200;

            const int maxAmount = 1;
            //Maximum Price from seeder is 150.
            var mockItem = new ChildCareFamilyProfileSeeder().Generate(maxAmount)[0];
            var additionalServicesList = new AdditionalServicesSeeder().Generate(maxAmount);
            var additionalTraitsList = new AdditionalTraitsSeeder().Generate(maxAmount);
            var availabilityList = new AvailabilitySeeder().Generate(maxAmount);

            authService.Setup(x => x.GetUserId(It.IsAny<ControllerBase>())).Returns("TestUserId");

            mockItem.UserId = "TestUserId";
            mockItem.AdditionalServicesId = additionalServicesList[0].Id;
            mockItem.AdditionalTraitsId = additionalTraitsList[0].Id;
            mockItem.AvailabilityId = availabilityList[0].Id;

            repository.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(mockItem);
            repository.Setup(x => x.GetDetails(It.IsAny<int>())).ReturnsAsync(mockItem);
            additionalServicesRepository.Setup(x => x.GetAsync(additionalServicesList[0].Id)).ReturnsAsync(additionalServicesList[0]);
            additionalTraitsRepository.Setup(x => x.GetAsync(additionalTraitsList[0].Id)).ReturnsAsync(additionalTraitsList[0]);
            availabilityRepository.Setup(x => x.GetAsync(availabilityList[0].Id)).ReturnsAsync(availabilityList[0]);

            var priceBeforeUpdate = controller.Get(mockItem.Id).Result.Value;
            Assert.NotNull(priceBeforeUpdate);
            Assert.NotEqual(expectedPrice, priceBeforeUpdate.Price);

            mockItem.Price = expectedPrice;

            var mappedChildCareFamilyProfile = mapper.Map<UpdateChildCareFamilyProfileDto>(mockItem);
            await controller.PutChildCareFamilyProfile(mockItem.Id, mappedChildCareFamilyProfile);

            repository.Verify(x => x.UpdateAsync(It.IsAny<ChildCareFamilyProfile>()), Times.Exactly(1));

            var actualPrice = controller.Get(mockItem.Id).Result.Value;
            Assert.NotNull(actualPrice);
            Assert.Equal(expectedPrice, actualPrice.Price);
        }
    }
}