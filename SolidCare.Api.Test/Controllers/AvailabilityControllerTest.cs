﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using SolidCare.API.Controllers;
using SolidCare.API.Core.Configurations;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.Availability;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Controllers
{
    public class AvailabilityControllerTest
    {
        private readonly Mock<IAvailabilityRepository> repository = new();
        private readonly Mock<IAuthenticationService> authService = new();
        private readonly IConfigurationProvider configProvider = new MapperConfiguration(config => config.AddProfile(new AutoMapperConfig()));
        private readonly AvailabilityController controller;

        public AvailabilityControllerTest()
        {
            IMapper mapper = new Mapper(configProvider);
            controller = new AvailabilityController(repository.Object, authService.Object, mapper);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            const int maxAmount = 5;
            var mockList = new AvailabilitySeeder().Generate(maxAmount);

            repository.Setup(x => x.GetAllAsync()).ReturnsAsync(mockList);

            var availabilityList = await controller.GetAll();

            Assert.NotNull(availabilityList);
            Assert.Equal(maxAmount * 2, availabilityList.Count());
            Assert.IsAssignableFrom<IEnumerable<GetAvailabilityDto>>(availabilityList);
        }

        [Fact]
        public async Task GetPaged_NoParameters_ReturnsEntitiesOfSelectedPage()
        {
            const int maxAmount = 5;
            var mockList = new AvailabilitySeeder().Generate(maxAmount);


            QueryParameters queryParameters = new QueryParameters { PageNumber = 1, PageSize = 2, StartIndex = 9 };

            repository.Setup(x => x.GetAllPagedAsync(queryParameters)).ReturnsAsync(new List<Availability>() { mockList[9] });

            var availabilityList = await controller.GetPaged(queryParameters);

            Assert.NotNull(availabilityList.Value);
            Assert.Single(availabilityList.Value.Items);
            Assert.IsAssignableFrom<IEnumerable<GetAvailabilityDto>>(availabilityList.Value.Items);
            Assert.IsAssignableFrom<PagedResult<GetAvailabilityDto>>(availabilityList.Value);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            const int maxAmount = 1;
            var mockItem = new AvailabilitySeeder().Generate(maxAmount)[0];

            repository.Setup(x => x.GetAsync(mockItem.Id)).ReturnsAsync(mockItem);

            var availabilityList = await controller.Get(mockItem.Id);

            Assert.NotNull(availabilityList.Value);
            Assert.Equal(mockItem.Id, availabilityList.Value.Id);
            Assert.IsAssignableFrom<GetAvailabilityDto>(availabilityList.Value);
        }
    }
}