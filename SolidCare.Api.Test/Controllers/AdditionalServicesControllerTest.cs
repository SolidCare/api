using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using SolidCare.API.Controllers;
using SolidCare.API.Core.Configurations;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.AdditionalServices;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using SolidCare.API.Data.Seeders;
using Xunit;

namespace SolidCare.Api.Test.Controllers
{
    public class AdditionalServicesControllerTest
    {
        private readonly Mock<IAdditionalServicesRepository> repository = new();
        private readonly Mock<IAuthenticationService> authService = new();
        private readonly IConfigurationProvider configProvider = new MapperConfiguration(config => config.AddProfile(new AutoMapperConfig()));
        private readonly AdditionalServicesController controller;

        public AdditionalServicesControllerTest()
        {
            IMapper mapper = new Mapper(configProvider);
            controller = new AdditionalServicesController(repository.Object, authService.Object, mapper);
        }

        [Fact]
        public async Task GetAll_NoParameters_ReturnsAllEntities()
        {
            const int maxAmount = 5;
            var mockList = new AdditionalServicesSeeder().Generate(maxAmount);

            repository.Setup(x => x.GetAllAsync()).ReturnsAsync(mockList);
            
            var additionalServicesList = await controller.GetAll();

            Assert.NotNull(additionalServicesList);
            Assert.Equal(maxAmount * 2, additionalServicesList.Count());
            Assert.IsAssignableFrom<IEnumerable<GetAdditionalServicesDto>>(additionalServicesList);
        }

        [Fact]
        public async Task GetPaged_NoParameters_ReturnsEntitiesOfSelectedPage()
        {
            const int maxAmount = 5;
            var mockList = new AdditionalServicesSeeder().Generate(maxAmount);

            QueryParameters queryParameters = new QueryParameters{PageNumber = 1, PageSize = 2, StartIndex = 9};
            
            repository.Setup(x => x.GetAllPagedAsync(queryParameters)).ReturnsAsync(new List<AdditionalServices>(){ mockList[9] });

            var additionalServicesList = await controller.GetPaged(queryParameters);

            Assert.NotNull(additionalServicesList.Value);
            Assert.Single(additionalServicesList.Value.Items);
            Assert.IsAssignableFrom<IEnumerable<GetAdditionalServicesDto>>(additionalServicesList.Value.Items);
            Assert.IsAssignableFrom<PagedResult<GetAdditionalServicesDto>>(additionalServicesList.Value);
        }

        [Fact]
        public async Task Get_NoParameters_ReturnsOneEntity()
        {
            const int maxAmount = 1;
            var mockItem = new AdditionalServicesSeeder().Generate(maxAmount)[0];

            repository.Setup(x => x.GetAsync(mockItem.Id)).ReturnsAsync(mockItem);

            var additionalServicesList = await controller.Get(mockItem.Id);

            Assert.NotNull(additionalServicesList.Value);
            Assert.Equal(mockItem.Id, additionalServicesList.Value.Id);
            Assert.IsAssignableFrom<GetAdditionalServicesDto>(additionalServicesList.Value);
        }
    }
}