using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.OData;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using SolidCare.API.Core.Authentication;
using SolidCare.API.Core.Configurations;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Middleware;
using SolidCare.API.Core.Repositories;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

var builder = WebApplication.CreateBuilder(args);

// Swagger/OpenAPI
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Setup DB Context
builder.Services.AddDbContext<ApiDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"), opts => opts.MigrationsAssembly("SolidCare.API.Core"));
});

//Identity Core
builder.Services.AddIdentityCore<ApiUser>()
    .AddRoles<IdentityRole>()
    .AddTokenProvider<DataProtectorTokenProvider<ApiUser>>("SolidCareApi")
    .AddEntityFrameworkStores<ApiDbContext>()
    .AddDefaultTokenProviders();

//JWT Authentication
builder.Services.AddAuthentication(options =>
{
    //Changes authentication to Bearer
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuerSigningKey = true,
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero,
        ValidIssuer = builder.Configuration["JwtSettings:Issuer"],
        ValidAudience = builder.Configuration["JwtSettings:Audience"],
        IssuerSigningKey = new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes(builder.Configuration["JwtSettings:Key"])
            )
    };
});

//Response cache
builder.Services.AddResponseCaching(options =>
    {
        //Cache size
        options.MaximumBodySize = 1024;
        //Path has to necessarily be the same case as specified in the controller.
        //Otherwise caching will not apply.
        options.UseCaseSensitivePaths = true;
    }
); 

// CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy(
        "AllowAll",
        pol
            => pol.AllowAnyHeader()
                .AllowAnyOrigin()
                .AllowAnyMethod());
});

//AutoMapper
builder.Services.AddAutoMapper(typeof(AutoMapperConfig));

//Repositories
builder.Services.AddScoped<IAuthManager, AuthManager>();
builder.Services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
builder.Services.AddScoped<IAdditionalServicesRepository, AdditionalServicesRepository>();
builder.Services.AddScoped<IAdditionalTraitsRepository, AdditionalTraitsRepository>();
builder.Services.AddScoped<IAvailabilityRepository, AvailabilityRepository>();
builder.Services.AddScoped<IChildCarePersonalProfileRepository, ChildCarePersonalProfileRepository>();
builder.Services.AddScoped<IChildCareFamilyProfileRepository, ChildCareFamilyProfileRepository>();

//General Services
builder.Services.AddSingleton<IAuthenticationService, AuthenticationService>();

//Logging
builder.Host.UseSerilog((context, loggingConfiguration)=>loggingConfiguration
    .WriteTo.Console()
    .ReadFrom.Configuration(context.Configuration)
);

//Controllers including OData
builder.Services.AddControllers().AddOData(options =>
{
    options.Select().Filter().OrderBy();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

//Global exception handling
app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseCors("AllowAll");

//Response cache
app.UseResponseCaching();
app.UseMiddleware<CachingMiddleware>();

app.UseAuthentication();
app.UseAuthorization();


app.MapControllers();

// Load DB Migrations to SQL Container
if (app.Environment.IsDevelopment())
{
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;

        //Use this for migrating container DB automatically
        var context = services.GetRequiredService<ApiDbContext>();
        context.Database.Migrate();
    }
}


app.Run();