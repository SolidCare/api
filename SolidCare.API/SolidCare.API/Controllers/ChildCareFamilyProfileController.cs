using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.ChildCareFamilyProfile;
using SolidCare.API.Core.Exceptions;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;
using IAuthenticationService = SolidCare.API.Core.Contracts.IAuthenticationService;

//using SolidCare.API.Core.Authentication;
//using AuthenticationService = SolidCare.API.Core.Authentication.AuthenticationService;

namespace SolidCare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildCareFamilyProfileController : ControllerBase
    {
        private readonly IChildCareFamilyProfileRepository repository;
        private readonly IAdditionalServicesRepository additionalServicesRepository;
        private readonly IAdditionalTraitsRepository additionalTraitsRepository;
        private readonly IAvailabilityRepository availabilityRepository;
        private readonly IAuthenticationService authenticationService;
        private readonly IMapper mapper;
        private readonly GenericController<ChildCareFamilyProfile> genericController;

        public ChildCareFamilyProfileController(
            IChildCareFamilyProfileRepository repository,
            IAdditionalServicesRepository additionalServicesRepository,
            IAdditionalTraitsRepository additionalTraitsRepository,
            IAvailabilityRepository availabilityRepository,
            IAuthenticationService authenticationService,
            IMapper mapper)
        {
            this.repository = repository;
            this.additionalServicesRepository = additionalServicesRepository;
            this.additionalTraitsRepository = additionalTraitsRepository;
            this.availabilityRepository = availabilityRepository;
            this.authenticationService = authenticationService;
            this.mapper = mapper;
            genericController = new GenericController<ChildCareFamilyProfile>(repository, authenticationService, mapper);
        }


        [HttpGet("GetAll")]
        [EnableQuery]
        public async Task<IEnumerable<GetAllChildCareFamilyProfileDto>> GetAll()
        {
            return await genericController.GetT<GetAllChildCareFamilyProfileDto>();
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<PagedResult<GetAllChildCareFamilyProfileDto>>> GetPaged([FromQuery] QueryParameters queryParameters)
        {
            return await genericController.GetPagedT<GetAllChildCareFamilyProfileDto>(queryParameters);
        }

        /// <summary>
        /// Gets ChildCareFamilyProfile according Id.
        /// The returned profile includes corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>ChildCareFamilyProfile including corresponding entities</returns>
        // GET: api/ChildCareFamilyProfile/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetChildCareFamilyProfileDto>> Get(int id)
        {
            var childCareFamilyProfile = await repository.GetDetails(id);

            if (childCareFamilyProfile == null)
            {
                throw new NotFoundException(id);
            }

            var childCareFamilyProfileDto = mapper.Map<GetChildCareFamilyProfileDto>(childCareFamilyProfile);

            return childCareFamilyProfileDto;
        }

        /// <summary>
        /// Updates ChildCareFamilyProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <param name="updateChildCareFamilyProfileDto">Profile including corresponding entities to update</param>
        /// <returns>No content response</returns>
        // PUT: api/ChildCareFamilyProfile/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutChildCareFamilyProfile(int id, UpdateChildCareFamilyProfileDto updateChildCareFamilyProfileDto)
        {
            var childCareFamilyProfile = await repository.GetAsync(id);
            var additionalServices = await additionalServicesRepository.GetAsync(childCareFamilyProfile.AdditionalServicesId);
            var additionalTraits = await additionalTraitsRepository.GetAsync(childCareFamilyProfile.AdditionalTraitsId);
            var availability = await availabilityRepository.GetAsync(childCareFamilyProfile.AvailabilityId);

            if (childCareFamilyProfile == null ||
                additionalServices == null ||
                additionalTraits == null ||
                availability == null)
            {
                throw new NotFoundException(id);
            }

            if (childCareFamilyProfile.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            mapper.Map(updateChildCareFamilyProfileDto, childCareFamilyProfile);
            mapper.Map(updateChildCareFamilyProfileDto.AdditionalServices, additionalServices);
            mapper.Map(updateChildCareFamilyProfileDto.AdditionalTraits, additionalTraits);
            mapper.Map(updateChildCareFamilyProfileDto.Availability, availability);

            try
            {
                await repository.UpdateAsync(childCareFamilyProfile);
                await additionalServicesRepository.UpdateAsync(additionalServices);
                await additionalTraitsRepository.UpdateAsync(additionalTraits);
                await availabilityRepository.UpdateAsync(availability);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await new GenericController<ChildCareFamilyProfile>(repository, authenticationService, mapper).Exist(id))
                {
                    throw new NotFoundException(id);
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Creates ChildCareFamilyProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="createChildCareFamilyProfileDto">Profile including corresponding entities to create</param>
        /// <returns>No content response</returns>
        // POST: api/ChildCareFamilyProfile
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostChildCareFamilyProfile(CreateChildCareFamilyProfileDto createChildCareFamilyProfileDto)
        {
            var childCareFamilyProfile = mapper.Map<ChildCareFamilyProfile>(createChildCareFamilyProfileDto);

            childCareFamilyProfile.UserId = authenticationService.GetUserId(this);

            await repository.AddAsync(childCareFamilyProfile);

            return NoContent();
        }

        /// <summary>
        /// Deletes ChildCareFamilyProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>No content response</returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var childCareFamilyProfile = await repository.GetAsync(id);

            if (childCareFamilyProfile == null)
            {
                throw new NotFoundException(id);
            }

            if (childCareFamilyProfile.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            await repository.DeleteAsync(id);
            await additionalServicesRepository.DeleteAsync(childCareFamilyProfile.AdditionalServicesId);
            await additionalTraitsRepository.DeleteAsync(childCareFamilyProfile.AdditionalTraitsId);
            await availabilityRepository.DeleteAsync(childCareFamilyProfile.AvailabilityId);

            return NoContent();
        }
    }
}
