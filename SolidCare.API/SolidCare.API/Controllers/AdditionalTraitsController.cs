﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.AdditionalTraits;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdditionalTraitsController 
    {

        private readonly GenericController<AdditionalTraits> genericController;
        public AdditionalTraitsController(IAdditionalTraitsRepository repository, IAuthenticationService authenticationService, IMapper mapper)
        {
            genericController = new GenericController<AdditionalTraits>(repository, authenticationService, mapper);
        }

        [HttpGet("GetAll")]
        [EnableQuery]
        public async Task<IEnumerable<GetAdditionalTraitsDto>> GetAll()
        {
            return await genericController.GetT<GetAdditionalTraitsDto>();
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<PagedResult<GetAdditionalTraitsDto>>> GetPaged([FromQuery] QueryParameters queryParameters)
        {
            return await genericController.GetPagedT<GetAdditionalTraitsDto>(queryParameters);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetAdditionalTraitsDto>> Get(int id)
        {
            return await genericController.GetT<GetAdditionalTraitsDto>(id);
        }
    }
}
