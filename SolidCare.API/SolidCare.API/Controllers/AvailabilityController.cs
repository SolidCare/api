using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.Availability;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvailabilityController 
    {
        private readonly GenericController<Availability> genericController;
        public AvailabilityController(IAvailabilityRepository repository, IAuthenticationService authenticationService, IMapper mapper)
        {
            genericController = new GenericController<Availability>(repository, authenticationService, mapper);
        }

        [HttpGet("GetAll")]
        [EnableQuery]
        public async Task<IEnumerable<GetAvailabilityDto>> GetAll()
        {
            return await genericController.GetT<GetAvailabilityDto>();
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<PagedResult<GetAvailabilityDto>>> GetPaged([FromQuery] QueryParameters queryParameters)
        {
            return await genericController.GetPagedT<GetAvailabilityDto>(queryParameters);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetAvailabilityDto>> Get(int id)
        {
            return await genericController.GetT<GetAvailabilityDto>(id);
        }
    }
}
