﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Exceptions;
using SolidCare.API.Core.Paging;

namespace SolidCare.API.Controllers
{
    public class GenericController<T> : ControllerBase where T : class
    {
        private readonly IGenericRepository<T> repository;
        private readonly IAuthenticationService authenticationService;
        private readonly IMapper mapper;

        public GenericController(IGenericRepository<T> repository, IAuthenticationService authenticationService, IMapper mapper)
        {
            this.repository = repository;
            this.authenticationService = authenticationService;
            this.mapper = mapper;
        }

        // GET: api/T/GetAll
        public virtual async Task<IEnumerable<TGetDto>> GetT<TGetDto>()
        {
            var receivedList = await repository.GetAllAsync();
            var mappedList = mapper.Map<List<TGetDto>>(receivedList);

            return mappedList;
        }

        // GET: api/T/?StartIndex=0&PageSize=15&PageNumber=1
        public virtual async Task<PagedResult<TGetDto>> GetPagedT<TGetDto>([FromQuery] QueryParameters queryParameters)
        {
            var pagedList = await repository.GetAllPagedAsync(queryParameters);
            var mappedPagedList = mapper.Map<List<TGetDto>>(pagedList);

            return new PagedResult<TGetDto>
            (
                await Count(), 
                queryParameters.PageSize,
                queryParameters.PageNumber,
                mappedPagedList
            );
        }

        // GET: api/T/5
        [HttpGet("{id}")]
        public virtual async Task<TGetDto> GetT<TGetDto>(int id)
        {
            var receivedObject = await repository.GetAsync(id);

            if (receivedObject == null)
            {
                throw new NotFoundException(id);
            }

            var mappedObject = mapper.Map<TGetDto>(receivedObject);

            return mappedObject;
        }

        // GET: api/T/5
        [HttpGet("{id}")]
        public virtual async Task<TGetDto> GetTAuth<TGetDto>(int id)
        {
            var receivedObject = await repository.GetAsyncAuth(id);

            if (receivedObject.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            if (receivedObject == null)
            {
                throw new NotFoundException(id);
            }

            var mappedObject = mapper.Map<TGetDto>(receivedObject);

            return mappedObject;
        }

        // PUT: api/T/5
        public virtual async Task<NoContentResult> PutT<TUpdateDto>(int id, TUpdateDto updateObjectDto)
        {
            var receivedObject = await repository.GetAsync(id);

            if (receivedObject == null)
            {
                throw new NotFoundException(id);
            }

            mapper.Map(updateObjectDto, receivedObject);

            try
            {
                await repository.UpdateAsync(receivedObject);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await Exist(id))
                {
                    throw new NotFoundException(id);
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/T
        public virtual async Task<NoContentResult> PostT<TCreateDto>(TCreateDto createObjectDto)
        {
            var receivedObject = mapper.Map<T>(createObjectDto);

            await repository.AddAsync(receivedObject);

            return NoContent();
        }

        // DELETE: api/T/5
        public virtual async Task<NoContentResult> DeleteT(int id)
        {
            var receivedObject = await repository.GetAsync(id);
            if (receivedObject == null)
            {
                throw new NotFoundException(id);
            }

            await repository.DeleteAsync(id);

            return NoContent();
        }

        // DELETE: api/T/5
        public virtual async Task<NoContentResult> DeleteTAuth(int id)
        {
            var receivedObject = await repository.GetAsyncAuth(id);
            if (receivedObject == null)
            {
                throw new NotFoundException(id);
            }

            if (receivedObject.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            await repository.DeleteAsync(id);

            return NoContent();
        }

        public async Task<bool> Exist(int id)
        {
            return await repository.Exists(id);
        }

        public async Task<int> Count()
        {
            return await repository.Count();
        }

    }
}

