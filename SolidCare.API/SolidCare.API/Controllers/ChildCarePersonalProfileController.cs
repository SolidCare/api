using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.ChildCarePersonalProfile;
using SolidCare.API.Core.Exceptions;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildCarePersonalProfileController : ControllerBase
    {
        private readonly IChildCarePersonalProfileRepository repository;
        private readonly IAdditionalServicesRepository additionalServicesRepository;
        private readonly IAdditionalTraitsRepository additionalTraitsRepository;
        private readonly IAvailabilityRepository availabilityRepository;
        private readonly IAuthenticationService authenticationService;
        private readonly IMapper mapper;
        private readonly GenericController<ChildCarePersonalProfile> genericController;

        public ChildCarePersonalProfileController(
            IChildCarePersonalProfileRepository repository,
            IAdditionalServicesRepository additionalServicesRepository,
            IAdditionalTraitsRepository additionalTraitsRepository,
            IAvailabilityRepository availabilityRepository,
            IAuthenticationService authenticationService,
            IMapper mapper)
        {
            this.repository = repository;
            this.additionalServicesRepository = additionalServicesRepository;
            this.additionalTraitsRepository = additionalTraitsRepository;
            this.availabilityRepository = availabilityRepository;
            this.authenticationService = authenticationService;
            this.mapper = mapper;
            genericController = new GenericController<ChildCarePersonalProfile>(repository, authenticationService, mapper);
        }


        [HttpGet("GetAll")]
        [EnableQuery]
        public async Task<IEnumerable<GetAllChildCarePersonalProfileDto>> GetAll()
        {
            return await genericController.GetT<GetAllChildCarePersonalProfileDto>();
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<PagedResult<GetAllChildCarePersonalProfileDto>>> GetPaged([FromQuery] QueryParameters queryParameters)
        {
            return await genericController.GetPagedT<GetAllChildCarePersonalProfileDto>(queryParameters);
        }

        /// <summary>
        /// Gets ChildCarePersonalProfile according Id.
        /// The returned profile includes corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>ChildCarePersonalProfile including corresponding entities</returns>
        // GET: api/ChildCarePersonalProfile/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetChildCarePersonalProfileDto>> Get(int id)
        {
            var childCarePersonalProfile = await repository.GetDetails(id);

            if (childCarePersonalProfile == null)
            {
                throw new NotFoundException(id);
            }

            var childCarePersonalProfileDto = mapper.Map<GetChildCarePersonalProfileDto>(childCarePersonalProfile);

            return childCarePersonalProfileDto;
        }

        /// <summary>
        /// Updates ChildCarePersonalProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <param name="updateChildCarePersonalProfileDto">Profile including corresponding entities to update</param>
        /// <returns>No content response</returns>
        // PUT: api/ChildCarePersonalProfile/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutChildCarePersonalProfile(int id, UpdateChildCarePersonalProfileDto updateChildCarePersonalProfileDto)
        {
            var childCarePersonalProfile = await repository.GetAsync(id);
            var additionalServices = await additionalServicesRepository.GetAsync(childCarePersonalProfile.AdditionalServicesId);
            var additionalTraits = await additionalTraitsRepository.GetAsync(childCarePersonalProfile.AdditionalTraitsId);
            var availability = await availabilityRepository.GetAsync(childCarePersonalProfile.AvailabilityId);

            if (childCarePersonalProfile == null ||
                additionalServices == null ||
                additionalTraits == null ||
                availability == null)
            {
                throw new NotFoundException(id);
            }

            if (childCarePersonalProfile.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            mapper.Map(updateChildCarePersonalProfileDto, childCarePersonalProfile);
            mapper.Map(updateChildCarePersonalProfileDto.AdditionalServices, additionalServices);
            mapper.Map(updateChildCarePersonalProfileDto.AdditionalTraits, additionalTraits);
            mapper.Map(updateChildCarePersonalProfileDto.Availability, availability);

            try
            {
                await repository.UpdateAsync(childCarePersonalProfile);
                await additionalServicesRepository.UpdateAsync(additionalServices);
                await additionalTraitsRepository.UpdateAsync(additionalTraits);
                await availabilityRepository.UpdateAsync(availability);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await genericController.Exist(id))
                {
                    throw new NotFoundException(id);
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        
        /// <summary>
        /// Creates ChildCarePersonalProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="createChildCarePersonalProfileDto">Profile including corresponding entities to create</param>
        /// <returns>No content response</returns>
        // POST: api/ChildCarePersonalProfile
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostChildCarePersonalProfile(CreateChildCarePersonalProfileDto createChildCarePersonalProfileDto)
        {
            var childCarePersonalProfile = mapper.Map<ChildCarePersonalProfile>(createChildCarePersonalProfileDto);

            childCarePersonalProfile.UserId = authenticationService.GetUserId(this);

            await repository.AddAsync(childCarePersonalProfile);

            return NoContent();
        }

        /// <summary>
        /// Deletes ChildCarePersonalProfile including corresponding AdditionalServices, AdditionalTraits and Availability entities.
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>No content response</returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            var childCarePersonalProfile = await repository.GetAsync(id);

            if (childCarePersonalProfile == null)
            {
                throw new NotFoundException(id);
            }

            if (childCarePersonalProfile.UserId != authenticationService.GetUserId(this))
            {
                throw new UnauthorizedAccessException();
            }

            await repository.DeleteAsync(id);
            await additionalServicesRepository.DeleteAsync(childCarePersonalProfile.AdditionalServicesId);
            await additionalTraitsRepository.DeleteAsync(childCarePersonalProfile.AdditionalTraitsId);
            await availabilityRepository.DeleteAsync(childCarePersonalProfile.AvailabilityId);

            return NoContent();
        }
    }
}
