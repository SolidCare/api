﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.AdditionalServices;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdditionalServicesController
    {
        private readonly GenericController<AdditionalServices> genericController;

        public AdditionalServicesController(
            IAdditionalServicesRepository repository,
            IAuthenticationService authenticationService,
            IMapper mapper)
        {
            genericController = new GenericController<AdditionalServices>(repository, authenticationService, mapper);
        }

        [HttpGet("GetAll")]
        [EnableQuery]
        public async Task<IEnumerable<GetAdditionalServicesDto>> GetAll()
        {
            return await genericController.GetT<GetAdditionalServicesDto>();
        }

        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<PagedResult<GetAdditionalServicesDto>>> GetPaged([FromQuery] QueryParameters queryParameters)
        {
            return await genericController.GetPagedT<GetAdditionalServicesDto>(queryParameters);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetAdditionalServicesDto>> Get(int id)
        {
            return await genericController.GetT<GetAdditionalServicesDto>(id);
        }
    }
}
