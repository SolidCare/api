﻿using Microsoft.AspNetCore.Mvc;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.User;
using SolidCare.API.Core.Exceptions;

namespace SolidCare.API.Controllers
{
    /// <summary>
    /// User authentication controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAuthManager authManager;
        private readonly ILogger<AccountController> logger;

        public AccountController(IAuthManager authManager, ILogger<AccountController> logger)
        {
            this.authManager = authManager;
            this.logger = logger;
        }

        /// <summary>
        /// User registration.
        /// Since this is not a generated Controller StatusCodes and FromBody has to be specified explicitly.
        /// Note URL extension /register
        /// </summary>
        /// <param name="apiUserDto"></param>
        /// <returns>Returns Error if unsuccessful</returns>
        //POST: api/Account/register
        [HttpPost]
        [Route("register")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> Register([FromBody] RegisterApiUserDto apiUserDto)
        {
            var errors = await authManager.Register(apiUserDto);

            //ReSharper requests an initialization of an List for errors.
            //For better readability this error is disabled.
            // ReSharper disable PossibleMultipleEnumeration
            if (!errors.Any()) return Ok();
            foreach (var error in errors)
            {
                ModelState.AddModelError(error.Code, error.Description);
            }
            // ReSharper restore PossibleMultipleEnumeration


            return BadRequest(ModelState);
        }

        /// <summary>
        /// User login.
        /// Since this is not a generated Controller StatusCodes and FromBody has to be specified explicitly.
        /// Note URL extension /login
        /// </summary>
        /// <param name="apiUserDto">Base user Dto</param>
        /// <returns>Successful login returns a Bearer Token | Unsuccessful returns Unauthorized</returns>
        //POST: api/Account/login
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> Login([FromBody] BaseApiUserDto apiUserDto)
        {
            logger.LogInformation($"Login attempt by {apiUserDto.Email}");

            var authResponse = await authManager.Login(apiUserDto);
            if (authResponse == null)
            {
                throw new NotFoundException(apiUserDto.Email);
            }

            return Ok(authResponse);
        }

        /// <summary>
        /// Renews the user Bearer and Refresh Token.
        /// Since this is not a generated Controller StatusCodes and FromBody has to be specified explicitly.
        /// Note URL extension /refreshtoken
        /// </summary>
        /// <param name="request">User Authentication Dto</param>
        /// <returns>Successful renewal returns a Bearer and Refresh Token. If unsuccessful returns Unauthorized</returns>
        //POST: api/Account/refreshtoken
        [HttpPost]
        [Route("refreshtoken")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> RefreshToken([FromBody] AuthResponseDto request)
        {
            var authResponse = await authManager.VerifyRefreshToken(request);
            if (authResponse == null)
            {
                throw new UnauthorizedException();
            }

            return Ok(authResponse);
        }
    }
}
