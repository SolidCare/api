# SolidCareAPI

## Name
SolidCareAPI - API Backend for SolidCare

## Project status
In active development.

## Description
SolidCare is a platform that enables families and people in need to find care personal.
First and foremost, the focus is on job placements for child care services. 
Later on other services such as pet care or elderly care are also to be made available.

The solution consists of a three project logicaly separated projects.
- Database: MS SQL Server 2019
- Backend API: ASP.NET 6
- Web-ui: Angular 13

Each project is deployed using docker containers.

## Visuals
TBD

## Installation
1. Clone Repository
2. Open Solution in VS
3. Run Docker Compose

### Requierements
Docker version 20.10.14, build a224086
Visual Studio 2022 Version 17.1.5

## Usage
TBD

## Test
TBD

## Support
TBD

## Roadmap
1. <input type="checkbox" disabled checked>Initial Setup 
2. <input type="checkbox" disabled checked>Docker Setup 
3. <input type="checkbox" disabled checked>EF Setup
4. <input type="checkbox" disabled checked>CRUD child care personal profile
5. <input type="checkbox" disabled checked>Authentication and authorization, complete User System
6. <input type="checkbox" disabled checked>CRUD User Profile
7. <input type="checkbox" disabled checked> Global Errorhandling
8. <input type="checkbox" disabled checked> Paging
9. <input type="checkbox" disabled checked> OData
10. <input type="checkbox" disabled>CRUD child care family profile
11. <input type="checkbox" disabled>User profile
12. <input type="checkbox" disabled> TBD

## Contributing
TBD

## Authors and acknowledgment
TBD

## License
TBD


