﻿using Microsoft.AspNetCore.Mvc;

namespace SolidCare.API.Core.Contracts
{
    public interface IAuthenticationService
    {
        public string GetUserId(ControllerBase controllerBase);
    }
}
