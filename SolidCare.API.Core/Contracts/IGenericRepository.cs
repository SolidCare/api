﻿using SolidCare.API.Core.Paging;
using SolidCare.API.Data.Contracts;

namespace SolidCare.API.Core.Contracts
{
    /// <summary>
    /// Contract base for the controller repository.
    /// </summary>
    /// <typeparam name="T">Specific Model</typeparam>
    public interface IGenericRepository<T> where T : class
    {
        Task<T> GetAsync(int? id);
        Task<IUserObject> GetAsyncAuth(int? id);
        Task<List<T>> GetAllAsync();
        Task<List<T>> GetAllPagedAsync(QueryParameters queryParameters);
        Task<T> AddAsync(T entity); 
        Task DeleteAsync(int id);
        Task UpdateAsync(T entity);
        Task UpdateAsync(IUserObject entity);
        Task<bool> Exists(int id);
        Task<int> Count();
    }
}
