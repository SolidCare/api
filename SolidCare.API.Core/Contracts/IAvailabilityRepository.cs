﻿using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Contracts
{
    public interface IAvailabilityRepository : IGenericRepository<Availability>{}
}
