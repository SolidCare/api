﻿using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Contracts
{
    public interface IAdditionalServicesRepository : IGenericRepository<AdditionalServices>{}
}
