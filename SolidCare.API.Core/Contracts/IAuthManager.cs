﻿using Microsoft.AspNetCore.Identity;
using SolidCare.API.Core.Dto.User;

namespace SolidCare.API.Core.Contracts
{
    /// <summary>
    /// Contract for user authentication.
    /// </summary>
    public interface IAuthManager
    {
        /// <summary>
        /// Handles user registration.
        /// </summary>
        /// <param name="userDto">User Dto containing Email, PW, Firstname and Lastname</param>
        /// <returns>Returns Error if registration was unsuccessful</returns>
        Task<IEnumerable<IdentityError>> Register(RegisterApiUserDto userDto);

        /// <summary>
        /// Handles user login.
        /// </summary>
        /// <param name="userDto">Base user Dto</param>
        /// <returns>Returns AuthResponseDto containing a Bearer Token</returns>
        Task<AuthResponseDto> Login(BaseApiUserDto userDto);

        /// <summary>
        /// Creates a new Refresh Token.
        /// </summary>
        /// <returns>Refresh Token</returns>
        Task<string> CreateRefreshToken();

        /// <summary>
        /// Verifies if given user is applicable for a new Refresh Token.
        /// </summary>
        /// <param name="request">User with Bearer Token and Refresh Token</param>
        /// <returns>If successful assigns new Bearer and Refresh Token. Save logout if unsuccessful</returns>
        Task<AuthResponseDto> VerifyRefreshToken(AuthResponseDto request);
    }
}
