using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Contracts
{
    public interface IChildCareFamilyProfileRepository : IGenericRepository<ChildCareFamilyProfile>
    {
        /// <summary>
        /// Gets Details of one ChildCareFamilyProfile by id.
        /// The ChildCareFamilyProfile includes the related entities AdditionalServices, AdditionalTraits and Availability.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ChildCareFamilyProfile including all related entities</returns>
        Task<ChildCareFamilyProfile> GetDetails(int? id);
    }
}