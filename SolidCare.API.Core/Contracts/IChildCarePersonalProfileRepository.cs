using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Contracts
{
    public interface IChildCarePersonalProfileRepository : IGenericRepository<ChildCarePersonalProfile>
    {
        /// <summary>
        /// Gets Details of one ChildCarePersonalProfile by id.
        /// The ChildCarePersonalProfile includes the related entities AdditionalServices, AdditionalTraits and Availability.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ChildCarePersonalProfile including all related entities</returns>
        Task<ChildCarePersonalProfile> GetDetails(int? id);
    }
}