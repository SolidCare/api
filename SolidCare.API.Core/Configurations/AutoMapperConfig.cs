﻿using AutoMapper;
using SolidCare.API.Core.Dto.AdditionalServices;
using SolidCare.API.Core.Dto.AdditionalTraits;
using SolidCare.API.Core.Dto.Availability;
using SolidCare.API.Core.Dto.ChildCareFamilyProfile;
using SolidCare.API.Core.Dto.ChildCarePersonalProfile;
using SolidCare.API.Core.Dto.User;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Configurations
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<AdditionalServices, CreateAdditionalServicesDto>().ReverseMap();
            CreateMap<AdditionalServices, GetAdditionalServicesDto>().ReverseMap();
            CreateMap<AdditionalServices, UpdateAdditionalServicesDto>().ReverseMap();

            CreateMap<AdditionalTraits, CreateAdditionalTraitsDto>().ReverseMap();
            CreateMap<AdditionalTraits, GetAdditionalTraitsDto>().ReverseMap();
            CreateMap<AdditionalTraits, UpdateAdditionalTraitsDto>().ReverseMap();

            CreateMap<Availability, CreateAvailabilityDto>().ReverseMap();
            CreateMap<Availability, GetAvailabilityDto>().ReverseMap();
            CreateMap<Availability, UpdateAvailabilityDto>().ReverseMap();

            CreateMap<ChildCarePersonalProfile, CreateChildCarePersonalProfileDto>().ReverseMap();
            CreateMap<ChildCarePersonalProfile, GetChildCarePersonalProfileDto>().ReverseMap();
            CreateMap<ChildCarePersonalProfile, GetAllChildCarePersonalProfileDto>().ReverseMap();
            CreateMap<ChildCarePersonalProfile, UpdateChildCarePersonalProfileDto>().ReverseMap();

            CreateMap<ChildCareFamilyProfile, CreateChildCareFamilyProfileDto>().ReverseMap();
            CreateMap<ChildCareFamilyProfile, GetChildCareFamilyProfileDto>().ReverseMap();
            CreateMap<ChildCareFamilyProfile, GetAllChildCareFamilyProfileDto>().ReverseMap();
            CreateMap<ChildCareFamilyProfile, UpdateChildCareFamilyProfileDto>().ReverseMap();

            CreateMap<ApiUser, RegisterApiUserDto>().ReverseMap();
            CreateMap<ApiUser, BaseApiUserDto>().ReverseMap();

        }
    }
}
