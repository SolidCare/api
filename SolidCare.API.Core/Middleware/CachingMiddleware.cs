﻿using Microsoft.AspNetCore.Http;

namespace SolidCare.API.Core.Middleware
{
    /// <summary>
    /// This middleware handles response caching.
    /// With response caching the connections to the Db is reduced.
    /// Therefore more memory is used by the server running the application.
    /// Select cacheTimer with care.
    /// The longer the timer the longer the data is cached the longer the user has to wait to get the updated list of entities.
    /// </summary>
    public class CachingMiddleware
    {
        private readonly RequestDelegate nextDelegate;

        public CachingMiddleware(RequestDelegate nextDelegate)
        {
            this.nextDelegate = nextDelegate;
        }

        /// <summary>
        /// Is invoked with each request delegate.
        /// Adds the caching time to the response.
        /// Also allows to cache a variety of data.
        /// </summary>
        /// <param name="context">Context containing http request</param>
        /// <returns>No return value</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            const int cacheTimerInSeconds = 5;

            context.Response.GetTypedHeaders().CacheControl =
                new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(cacheTimerInSeconds)
                };
            //Allows to cache a variety of data.
            context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
                new [] { "Accept-Encoding" };

            await nextDelegate(context);
        }
    }
}
