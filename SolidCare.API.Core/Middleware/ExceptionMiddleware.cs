﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SolidCare.API.Core.Dto.ErrorAndExceptions;
using SolidCare.API.Core.Exceptions;

namespace SolidCare.API.Core.Middleware
{
    /// <summary>
    /// Exception middleware acts as a global error and exception handler.
    /// Logs the process a exception was caught.
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate nextDelegate;
        private readonly ILogger<ExceptionMiddleware> logger;

        public ExceptionMiddleware(RequestDelegate nextDelegate, ILogger<ExceptionMiddleware> logger)
        {
            this.nextDelegate = nextDelegate;
            this.logger = logger;
        }

        /// <summary>
        /// Tries to run next delegate.
        /// If an exception is caught logs the request path and invokes the exception handler.
        /// </summary>
        /// <param name="context">Context containing http request</param>
        /// <returns>No return value</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await nextDelegate(context);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Exception thrown during process {context.Request.Path}");
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Returns a http response containing the http statusCode and a descriptive body.
        /// Handles response according to defined exceptions.
        /// If no specific exception is specified returns general ErrorType "Failure" with caught message.
        /// </summary>
        /// <param name="context">Context containing http request</param>
        /// <param name="ex">Specific exception</param>
        /// <returns>http response containing exception message</returns>
        private Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            context.Response.ContentType = "application/json";

            //Default error. If no http Status code is available it makes most sense to return an InternalServerError.
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;

            var errorDetails = new ErrorDetails
            {
                ErrorType = "Failure",
                ErrorMessage = ex.Message
            };

            switch (ex)
            {
                case NotFoundException:
                    statusCode = HttpStatusCode.NotFound;
                    errorDetails.ErrorType = "Not Found";
                    break;
                case BadRequestException:
                    statusCode = HttpStatusCode.BadRequest;
                    errorDetails.ErrorType = "Bad Request";
                    break;
                case UnauthorizedException:
                    statusCode = HttpStatusCode.Unauthorized;
                    errorDetails.ErrorType = "Unauthorized";
                    break;
            }

            string response = JsonConvert.SerializeObject(errorDetails);
            context.Response.StatusCode = (int)statusCode;
            return context.Response.WriteAsync(response);
        }
    }
}
