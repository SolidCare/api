﻿using Microsoft.AspNetCore.Mvc;
using SolidCare.API.Core.Contracts;

namespace SolidCare.API.Core.Authentication
{
    /// <summary>
    /// Extends ControllerBase with authentication functionality.
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Gets the user id from the user claims.
        /// Used for authentication of objects by user id.
        /// </summary>
        /// <returns>User id</returns>
        public string GetUserId(ControllerBase controllerBase)
        {
            return controllerBase.User.Claims.First(claim => claim.Type == "uid").Value;
        }
    }
}
