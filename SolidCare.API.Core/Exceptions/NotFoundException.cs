﻿using System.Runtime.CompilerServices;

namespace SolidCare.API.Core.Exceptions
{    
    /// <summary>
    /// Not Found exception to overwrite NotFound http response.
    /// </summary>
    public class NotFoundException : ApplicationException
    {
        public NotFoundException(object key, [CallerMemberName] string name = "" ) : base($"{name} ({key}) was not found")
        {
            
        }
    }
}
