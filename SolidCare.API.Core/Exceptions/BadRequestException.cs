﻿using System.Runtime.CompilerServices;

namespace SolidCare.API.Core.Exceptions
{
    /// <summary>
    /// Bad Request exception to overwrite BadRequest http response.
    /// </summary>
    public class BadRequestException: ApplicationException
    {
        public BadRequestException([CallerMemberName] string name = "") : base($"{name} Request contains insufficient parameters") {}
    }
}
