﻿using System.Runtime.CompilerServices;

namespace SolidCare.API.Core.Exceptions
{
    /// <summary>
    /// Unauthorized exception to overwrite Unauthorized http response.
    /// </summary>
    public class UnauthorizedException : ApplicationException
    {
        public UnauthorizedException([CallerMemberName] string name = "" ) : base($"{name} Not authorized. Please try to login or register a new account")
        {
            
        }
    }
}
