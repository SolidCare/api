﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SolidCare.API.Core.Migrations
{
    public partial class inital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdditionalServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cook = table.Column<bool>(type: "bit", nullable: false),
                    HelpWithHomework = table.Column<bool>(type: "bit", nullable: false),
                    HelpWithHousekeeping = table.Column<bool>(type: "bit", nullable: false),
                    EscortChild = table.Column<bool>(type: "bit", nullable: false),
                    MedicationDispensing = table.Column<bool>(type: "bit", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalServices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AdditionalTraits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Smoker = table.Column<bool>(type: "bit", nullable: false),
                    AcceptsPets = table.Column<bool>(type: "bit", nullable: false),
                    CarLicense = table.Column<bool>(type: "bit", nullable: false),
                    FirstAid = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdditionalTraits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Firstname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Availability",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AvailableFrom = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Frequency = table.Column<int>(type: "int", nullable: false),
                    Workinghours = table.Column<int>(type: "int", nullable: false),
                    WorkinghoursFrequency = table.Column<int>(type: "int", nullable: false),
                    MondayMorning = table.Column<bool>(type: "bit", nullable: false),
                    MondayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    MondayEvening = table.Column<bool>(type: "bit", nullable: false),
                    MondayNight = table.Column<bool>(type: "bit", nullable: false),
                    TuesdayMorning = table.Column<bool>(type: "bit", nullable: false),
                    TuesdayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    TuesdayEvening = table.Column<bool>(type: "bit", nullable: false),
                    TuesdayNight = table.Column<bool>(type: "bit", nullable: false),
                    WednesdayMorning = table.Column<bool>(type: "bit", nullable: false),
                    WednesdayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    WednesdayEvening = table.Column<bool>(type: "bit", nullable: false),
                    WednesdayNight = table.Column<bool>(type: "bit", nullable: false),
                    ThursdayMorning = table.Column<bool>(type: "bit", nullable: false),
                    ThursdayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    ThursdayEvening = table.Column<bool>(type: "bit", nullable: false),
                    ThursdayNight = table.Column<bool>(type: "bit", nullable: false),
                    FridayMorning = table.Column<bool>(type: "bit", nullable: false),
                    FridayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    FridayEvening = table.Column<bool>(type: "bit", nullable: false),
                    FridayNight = table.Column<bool>(type: "bit", nullable: false),
                    SaturdayMorning = table.Column<bool>(type: "bit", nullable: false),
                    SaturdayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    SaturdayEvening = table.Column<bool>(type: "bit", nullable: false),
                    SaturdayNight = table.Column<bool>(type: "bit", nullable: false),
                    SundayMorning = table.Column<bool>(type: "bit", nullable: false),
                    SundayAfternoon = table.Column<bool>(type: "bit", nullable: false),
                    SundayEvening = table.Column<bool>(type: "bit", nullable: false),
                    SundayNight = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Availability", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChildCareFamilyProfile",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Place = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ChildrenAmount = table.Column<int>(type: "int", nullable: false),
                    CareSubType = table.Column<int>(type: "int", nullable: false),
                    AgeGroup = table.Column<int>(type: "int", nullable: false),
                    AdditionalServicesId = table.Column<int>(type: "int", nullable: false),
                    AdditionalTraitsId = table.Column<int>(type: "int", nullable: false),
                    AvailabilityId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChildCareFamilyProfile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChildCareFamilyProfile_AdditionalServices_AdditionalServicesId",
                        column: x => x.AdditionalServicesId,
                        principalTable: "AdditionalServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChildCareFamilyProfile_AdditionalTraits_AdditionalTraitsId",
                        column: x => x.AdditionalTraitsId,
                        principalTable: "AdditionalTraits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChildCareFamilyProfile_Availability_AvailabilityId",
                        column: x => x.AvailabilityId,
                        principalTable: "Availability",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChildCarePersonalProfile",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Place = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxChildren = table.Column<int>(type: "int", nullable: false),
                    Experience = table.Column<int>(type: "int", nullable: false),
                    CareSubType = table.Column<int>(type: "int", nullable: false),
                    AgeGroup = table.Column<int>(type: "int", nullable: false),
                    AdditionalServicesId = table.Column<int>(type: "int", nullable: false),
                    AdditionalTraitsId = table.Column<int>(type: "int", nullable: false),
                    AvailabilityId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChildCarePersonalProfile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChildCarePersonalProfile_AdditionalServices_AdditionalServicesId",
                        column: x => x.AdditionalServicesId,
                        principalTable: "AdditionalServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChildCarePersonalProfile_AdditionalTraits_AdditionalTraitsId",
                        column: x => x.AdditionalTraitsId,
                        principalTable: "AdditionalTraits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChildCarePersonalProfile_Availability_AvailabilityId",
                        column: x => x.AvailabilityId,
                        principalTable: "Availability",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AdditionalServices",
                columns: new[] { "Id", "Cook", "EscortChild", "HelpWithHomework", "HelpWithHousekeeping", "MedicationDispensing", "UserId" },
                values: new object[,]
                {
                    { 1, true, true, true, false, true, null },
                    { 2, false, false, false, false, false, null },
                    { 3, false, false, false, false, true, null },
                    { 4, false, false, false, true, false, null },
                    { 5, true, false, false, false, false, null },
                    { 6, false, false, false, true, true, null },
                    { 7, false, false, false, true, false, null },
                    { 8, false, true, false, true, false, null },
                    { 9, true, false, false, false, true, null },
                    { 10, true, false, false, true, false, null }
                });

            migrationBuilder.InsertData(
                table: "AdditionalTraits",
                columns: new[] { "Id", "AcceptsPets", "CarLicense", "FirstAid", "Smoker" },
                values: new object[,]
                {
                    { 1, true, false, false, false },
                    { 2, true, false, true, false },
                    { 3, false, false, true, true },
                    { 4, false, false, true, true },
                    { 5, false, true, false, false },
                    { 6, false, true, true, true },
                    { 7, true, true, true, false },
                    { 8, true, true, false, false },
                    { 9, true, false, false, true },
                    { 10, true, false, false, false }
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "4ba58542-81a6-4285-983b-af62d54937d1", "0b37b6d9-613c-4080-8330-f1231418b838", "User", "USER" },
                    { "91816955-d40d-431d-bc62-571b852389d1", "59a87acb-9df3-40dd-afe8-b71b79c54884", "Administrator", "ADMINISTRATOR" }
                });

            migrationBuilder.InsertData(
                table: "Availability",
                columns: new[] { "Id", "AvailableFrom", "Frequency", "FridayAfternoon", "FridayEvening", "FridayMorning", "FridayNight", "MondayAfternoon", "MondayEvening", "MondayMorning", "MondayNight", "SaturdayAfternoon", "SaturdayEvening", "SaturdayMorning", "SaturdayNight", "SundayAfternoon", "SundayEvening", "SundayMorning", "SundayNight", "ThursdayAfternoon", "ThursdayEvening", "ThursdayMorning", "ThursdayNight", "TuesdayAfternoon", "TuesdayEvening", "TuesdayMorning", "TuesdayNight", "WednesdayAfternoon", "WednesdayEvening", "WednesdayMorning", "WednesdayNight", "Workinghours", "WorkinghoursFrequency" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 5, 18, 22, 31, 0, 986, DateTimeKind.Local).AddTicks(68), 2, false, false, false, false, true, false, true, false, true, false, false, false, false, false, true, true, false, false, false, true, false, true, true, false, false, false, false, false, 9, 0 },
                    { 2, new DateTime(2022, 5, 18, 18, 27, 56, 101, DateTimeKind.Local).AddTicks(3338), 1, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, 10, 2 },
                    { 3, new DateTime(2022, 5, 19, 10, 39, 6, 129, DateTimeKind.Local).AddTicks(9541), 2, false, false, true, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, true, false, false, 4, 0 },
                    { 4, new DateTime(2022, 5, 19, 7, 10, 18, 419, DateTimeKind.Local).AddTicks(6599), 0, false, false, false, false, true, false, false, false, true, true, true, false, false, true, true, false, true, false, false, true, true, true, true, false, false, false, false, true, 6, 1 },
                    { 5, new DateTime(2022, 5, 19, 14, 15, 15, 238, DateTimeKind.Local).AddTicks(9891), 2, false, true, false, false, false, false, false, true, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, false, true, true, 12, 1 },
                    { 6, new DateTime(2022, 5, 18, 22, 53, 26, 286, DateTimeKind.Local).AddTicks(3132), 0, false, false, false, false, false, false, false, true, true, true, false, true, false, false, false, false, false, false, true, false, true, false, true, false, false, false, false, false, 10, 0 },
                    { 7, new DateTime(2022, 5, 19, 13, 26, 22, 929, DateTimeKind.Local).AddTicks(6120), 2, true, false, false, true, false, false, false, false, true, false, false, true, false, false, false, false, false, false, true, false, true, false, true, false, false, false, false, false, 12, 2 },
                    { 8, new DateTime(2022, 5, 18, 20, 49, 47, 116, DateTimeKind.Local).AddTicks(2549), 2, false, false, false, false, false, false, true, false, false, false, false, true, false, false, false, true, false, false, true, false, true, false, false, false, false, false, false, false, 0, 0 },
                    { 9, new DateTime(2022, 5, 19, 7, 22, 58, 941, DateTimeKind.Local).AddTicks(1140), 1, false, false, false, false, false, false, true, false, false, false, true, false, true, false, false, false, true, true, false, true, true, false, true, true, false, false, true, true, 5, 0 },
                    { 10, new DateTime(2022, 5, 19, 7, 24, 19, 127, DateTimeKind.Local).AddTicks(9793), 2, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, true, true, true, false, false, false, false, false, false, true, true, false, true, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "ChildCareFamilyProfile",
                columns: new[] { "Id", "AdditionalServicesId", "AdditionalTraitsId", "AgeGroup", "AvailabilityId", "CareSubType", "ChildrenAmount", "City", "Description", "Place", "Price", "Title", "UserId" },
                values: new object[,]
                {
                    { 1, 6, 6, 3, 6, 0, 5, "Lake Jasmin", "Debitis voluptates quo. Quisquam iure veritatis sint ea quasi ducimus. Repudiandae pariatur eius debitis dolor officia. Tempora et harum.", "20542-7690", 47.42m, "Laudantium eaque et.", "APA91uVmmmconAsJ3PY3es698Skj4GTyyHiOr1mcjJXciWP-pvAW93GPUVVIt4ZxBypo2BWwceqFww_qYXVLncLWws4z06X9BM1ariDZRc64EkV9FI-lctOZj-zGF5m0KuKJjoRX-fg1ZRRqCFLPuB7Q8zYbgLJMRvbEltT5-fNFRntsMCNzaGD" },
                    { 2, 7, 7, 3, 7, 2, 7, "Nyasiafort", "Voluptas non laboriosam et laborum possimus. Amet sunt porro voluptas possimus eum modi quo dolorem. Quis saepe voluptatibus dolores repellat. Nobis aliquam labore exercitationem corrupti illum. Nihil explicabo ut est dolores non. Distinctio perspiciatis cupiditate molestiae corrupti.", "30334-9159", 135.85m, "Est quia expedita.", "APA91nyzJ4QCbR50Z99CRhGkDXgVtqKy5VNTW7L8s_Mzlx_20zv4iQ5fM9sBnxNU66Z5wPjlclTVtu5VKF9CIW9g4M6BVgfgOSjVQysor48xFOMvXiCcnVx3GHKwgtoutnv558DjAnqjkiDzNNy2fsUnIbsHXmB39km2orV4R6yUC2Ul_zMDTEw" },
                    { 3, 8, 8, 2, 8, 1, 5, "West Josueland", "Aliquid aut qui et ullam porro et voluptatem soluta dolorum. Ut eveniet optio repudiandae asperiores enim ipsa. Odio sed facere nulla et aut libero a corrupti.", "80250-3077", 89.37m, "Eaque et assumenda.", "APA91ZbXZthnGA297uUBrbs4hO3qBJ3DjD4mSMYx6x8hGPe6ssx-0JQ4vF6KGcLhwQyhKIWnXiRgW0qvNBD1BDUUDS1cpyw-bxn45Sv66UpIxv7v5mCnYjNDqjiphstvh2oXZzyv7tUDOU98TL1-7vcsuLxvqk9nLr7lm0fEDsjVptH7JXh_RAX" },
                    { 4, 9, 9, 3, 9, 0, 5, "West Natashamouth", "Provident esse alias consequuntur inventore sunt. Provident similique repellendus. Sunt voluptas nam et est. Nesciunt alias libero molestiae aut in repellendus. Dolorem hic eius accusantium expedita delectus ut.", "12059-7577", 75.98m, "Nesciunt cupiditate recusandae.", "APA91ZU1pyyHoA6g7VjAIjVEaEhAfF8IsucChwJ-20P7V_ZNfYHLkm-RbCXB-8Vxe_4EpahJXtgS4rPq2jaViZ_gye8hnmIFkTEX3vtIAyW3IEpym6sPDaPhxoq_OwDB22UEuFWO4FwC4ORxrt2HTMB4mgWLKM0U2B2ZcZ0zPeUnA8WclwJvH0B" },
                    { 5, 10, 10, 4, 10, 1, 3, "Deondreside", "Sunt sed atque architecto et voluptatem reiciendis esse illum. Ut sint qui nam culpa beatae laudantium doloribus praesentium aspernatur. Eos fugiat neque veniam necessitatibus.", "47343", 88.82m, "Inventore maxime maxime.", "APA91gHk754MLf68QFkA3PNY00VjU22FJaJ52a2fm35wNkn1iCG3_uX9QKG2V2NSzeN3zYcIC62i0w_RKRI0n67XZ6VZXPrEGnKZjlpYLAkOcjn3Av0qS-M5QmrvGoPX1U3SLWDFqvY-tRAMPNHHglgNKklOLqPl5leFFmb5sRzSr6H7DWI_etm" }
                });

            migrationBuilder.InsertData(
                table: "ChildCarePersonalProfile",
                columns: new[] { "Id", "AdditionalServicesId", "AdditionalTraitsId", "AgeGroup", "AvailabilityId", "CareSubType", "City", "Description", "Experience", "MaxChildren", "Place", "Price", "Title", "UserId" },
                values: new object[,]
                {
                    { 1, 1, 1, 4, 1, 1, "New Dudleyfurt", "Consequatur voluptas ea et. Voluptas voluptatum aut. Cupiditate tenetur sapiente.", 18, 7, "92555", 40.10m, "Et eaque hic.", "APA912NyY614setI6Z50NgcRAM74GjHEwiKDi55e3Dy8CNS0KaBEQyxRwJWDg2EUZhVu1vuptbZ_lk2iLPNk6TBQGTCMmM5UNzCekB8GDmUCe5Ps4os4ez-36xfSAV1HEQ2-iwZVOBNlqEB3BAW5PMYNYvXf30Uex5xxAq5ree10SrrC1G9F6pa" },
                    { 2, 2, 2, 1, 2, 1, "South Monserrate", "Corporis porro consequatur amet quia expedita eaque nulla et. Repellat eos doloribus labore omnis. Sapiente maxime ea porro ut quisquam natus rerum est.", 17, 4, "77904-1573", 142.40m, "Odio ut sunt.", "APA91pbH0pIb6Ax0shAbfG2nST0s-n0vIE_5QZMBL8zfoRI_rkBcwymkwmknicvQ5Lze_bGgsSf8kSwOqYxven3LT2ic3FLPsKFfyRR0DkwkPRU59hn498qIpZWriWhbxOBsLeg1CBMp3AHxnjYyUCGsF5kQFYKW9TbLOe3xuSKgQBKNGY_Rjv8" },
                    { 3, 3, 3, 0, 3, 0, "Ronburgh", "Nesciunt totam non dolorum eveniet et sunt. Fuga magni provident non. Rerum nihil eius voluptas non qui porro. Similique autem ut eum nisi incidunt veniam voluptatem laudantium.", 1, 2, "46414", 133.15m, "Quo sit sequi.", "APA9177Se2mUXNXM8ZhforGMFIEsQXXuYful5g1OfT6U_RnE95xgvmHUePB7uzLWcZil28sK-g34KA3v5R0IoOeMpSBoNSsR0XWyjSJ-pBcMEAwKpg1PmCVwNSm9anlCAQIzLH2GgiaK5cE-huAmLPxv4nZEQeaoTsBhNOlmZFuVfsDFzOs0oK1" },
                    { 4, 4, 4, 2, 4, 0, "Myrticeburgh", "Eum accusamus debitis veritatis exercitationem ad suscipit saepe. Quia adipisci architecto qui quasi iusto omnis fuga. Nihil atque voluptatem modi voluptas velit. Quia necessitatibus numquam reiciendis architecto ut eos porro qui. Aut at aspernatur voluptates aperiam inventore. Fugit et molestiae aut enim sed incidunt nihil incidunt recusandae.", 21, 6, "33156", 117.26m, "Dignissimos quam veniam.", "APA91FrnN_WQ3L9u6lv9McuNbLNjlfhekTmSKDi_B6gYlg5CoKQhfGoveG0Ju23tusfoRNNfZR5ZHm-Q4qhpXF8guxJ0uWVS54FoSBl7WZ8zku4xiMHGWzX_YcOLz6h_LImTunnpMNmj_bq-_IcwGvPVQlKrEiiQ65uoS5pKtMZIF8D_UQxiENs" },
                    { 5, 5, 5, 3, 5, 2, "Lake Aldaborough", "Voluptas et aut explicabo ut. Odit cupiditate voluptas odit autem qui facere mollitia aut molestiae. Molestiae deserunt eveniet et illum. Maiores itaque tenetur expedita odit eaque modi. Et accusantium fugit alias aliquam. Quo provident eum nisi natus.", 1, 4, "31810-7693", 32.32m, "Officiis ea cumque.", "APA91H3sDpoygz1ojGa978tUpGgU_rhE8tg_7t3QaG9p5Dgj80s1jXetvus5XvOT8Bs6T9venfzgAZx76JllvJybZVywjKre1b3lN_hBVMz6gCnwcARngV20_2g7s3j63uA0UlMG7CXvhFiIjP4EDekawe37UNW-uE7lGil5kL__i6kPV7HLBVa" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCareFamilyProfile_AdditionalServicesId",
                table: "ChildCareFamilyProfile",
                column: "AdditionalServicesId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCareFamilyProfile_AdditionalTraitsId",
                table: "ChildCareFamilyProfile",
                column: "AdditionalTraitsId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCareFamilyProfile_AvailabilityId",
                table: "ChildCareFamilyProfile",
                column: "AvailabilityId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCarePersonalProfile_AdditionalServicesId",
                table: "ChildCarePersonalProfile",
                column: "AdditionalServicesId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCarePersonalProfile_AdditionalTraitsId",
                table: "ChildCarePersonalProfile",
                column: "AdditionalTraitsId");

            migrationBuilder.CreateIndex(
                name: "IX_ChildCarePersonalProfile_AvailabilityId",
                table: "ChildCarePersonalProfile",
                column: "AvailabilityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ChildCareFamilyProfile");

            migrationBuilder.DropTable(
                name: "ChildCarePersonalProfile");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "AdditionalServices");

            migrationBuilder.DropTable(
                name: "AdditionalTraits");

            migrationBuilder.DropTable(
                name: "Availability");
        }
    }
}
