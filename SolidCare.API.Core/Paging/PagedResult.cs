﻿namespace SolidCare.API.Core.Paging
{
    /// <summary>
    /// Model for paged http response.
    /// Holds a filtered list of entities.
    /// Also holds information about the total amount of entities,
    /// on what page the api consumer is on and the amount of entities on the current page.
    /// </summary>
    /// <typeparam name="T">Entity to return</typeparam>
    public class PagedResult<T>
    {
        public int PageNumber { get; set; }
        public int PageMaxNum { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public List<T> Items { get; set; }

        public PagedResult(int totalCount, int pageSize, int pageNumber, List<T> items)
        {
            TotalCount = totalCount;
            PageSize = pageSize;
            PageNumber = pageNumber;
            PageMaxNum = CalcMaxPageNum(totalCount, pageSize);
            Items = items;
        }

        /// <summary>
        /// Calculates the amount of pages available.
        /// </summary>
        /// <param name="totalCount">Total number of objects</param>
        /// <param name="pageSize">Objects per page</param>
        /// <returns>Total amount of pages</returns>
        public int CalcMaxPageNum(int totalCount, int pageSize)
        {
            try
            {
                return (int)Math.Ceiling((decimal)totalCount / pageSize);

            }
            catch (DivideByZeroException)
            {
                return 0;
            }
        }
    }
}
