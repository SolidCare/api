﻿namespace SolidCare.API.Core.Paging
{
    /// <summary>
    /// Set query parameters which can be used to filter and restrain the paging response.
    /// StartIndex determines from which entities the paging starts.
    /// PageNumber displays the current page number.
    /// </summary>
    public class QueryParameters
    {
        public int StartIndex { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 3;
    }
}
