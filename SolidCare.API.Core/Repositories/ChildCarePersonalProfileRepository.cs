using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Repositories
{
    public class ChildCarePersonalProfileRepository : GenericRepository<ChildCarePersonalProfile> , IChildCarePersonalProfileRepository
    {
        private readonly ApiDbContext context;

        public ChildCarePersonalProfileRepository(ApiDbContext context) : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// Displays one ChildCarePersonalProfile by Id
        /// Includes all related entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ChildCarePersonalProfile including all related entities</returns>
        public async Task<ChildCarePersonalProfile> GetDetails(int? id)
        {
            return await context.ChildCarePersonalProfile.
                Include(addServ => addServ.AdditionalServices)
                .Include(addTrait => addTrait.AdditionalTraits)
                .Include(avail => avail.Availability)
                .FirstOrDefaultAsync(profile => profile.Id == id);
        }
    }
}