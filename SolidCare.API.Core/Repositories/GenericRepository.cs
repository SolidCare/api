﻿using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Paging;
using SolidCare.API.Data;
using SolidCare.API.Data.Contracts;

namespace SolidCare.API.Core.Repositories
{
    /// <summary>
    /// Repository base for the controller repositories.
    /// Contains specific abstracted implementation of controller CRUD.
    /// </summary>
    /// <typeparam name="T">Specific Model</typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly ApiDbContext context;

        public GenericRepository(ApiDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets list of entities from Db according filtered by query parameters.
        /// Used for paging.
        /// </summary>
        /// <param name="queryParameters">Paging parameters</param>
        /// <returns>List of entities</returns>
        public async Task<List<T>> GetAllPagedAsync(QueryParameters queryParameters)
        {
            return await context.Set<T>()
                .Skip(queryParameters.StartIndex + queryParameters.PageSize * (queryParameters.PageNumber -1))
                .Take(queryParameters.PageSize)
                .ToListAsync();
        }

        public async Task<T> AddAsync(T entity)
        {
            await context.AddAsync(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await GetAsync(id);
            context.Entry(entity).State = EntityState.Deleted;
            await context.SaveChangesAsync();
        }

        public async Task<bool> Exists(int id)
        {
            var entity = await GetAsync(id);
            return entity != null; 

        }

        public async Task<int> Count()
        {
            return await context.Set<T>().CountAsync();
        }

        public async Task<List<T>> GetAllAsync()
        {
            return await context.Set<T>().ToListAsync();
        }

        public async Task<T> GetAsync(int? id)
        {
            if (id == null)
            {
                return null;
            }

            return await context.Set<T>().FindAsync(id);
        }

        /// <summary>
        /// Use this method for objects that hold a UserId.
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Entity of type IUserObject</returns>
        public async Task<IUserObject> GetAsyncAuth(int? id)
        {
            if (id == null)
            {
                return null;
            }

            return await context.Set<IUserObject>().FindAsync(id);
        }

        public async Task UpdateAsync(T entity)
        {
            context.Update(entity);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Use this method for objects that hold a UserId.
        /// </summary>
        /// <param name="entity">Entity of type IUserObject</param>
        public async Task UpdateAsync(IUserObject entity)
        {
            context.Update(entity);
            await context.SaveChangesAsync();
        }
    }
}
