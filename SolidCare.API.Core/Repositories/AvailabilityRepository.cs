﻿using SolidCare.API.Core.Contracts;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Repositories
{
    public class AvailabilityRepository : GenericRepository<Availability> , IAvailabilityRepository
    {
        public AvailabilityRepository(ApiDbContext context) : base(context)
        {
        }
    }
}
