﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Core.Dto.User;
using SolidCare.API.Data.Models;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace SolidCare.API.Core.Repositories
{
    /// <summary>
    /// User authentication Repository.
    /// </summary>
    public class AuthManager : IAuthManager
    {
        private readonly IMapper mapper;
        private readonly UserManager<ApiUser> userManager;
        private readonly IConfiguration configuration;
        private ApiUser user;
        private const string LoginProvider = "SolidCareApi";
        private const string RefreshToken = "RefreshToken";

        public AuthManager(IMapper mapper, UserManager<ApiUser> userManager, IConfiguration configuration)
        {
            this.mapper = mapper;
            this.userManager = userManager;
            this.configuration = configuration;
        }

        /// <summary>
        /// Handles user registration.
        /// </summary>
        /// <param name="userDto">Register User Dto</param>
        /// <returns>Returns Error if registration was unsuccessful</returns>
        public async Task<IEnumerable<IdentityError>> Register(RegisterApiUserDto userDto)
        {
            user = mapper.Map<ApiUser>(userDto);

            user.UserName = userDto.Email;

            var result = await userManager.CreateAsync(user, userDto.Password);

            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(user, "User");
            }

            return result.Errors;
        }

        /// <summary>
        /// Handles user login.
        /// </summary>
        /// <param name="userDto">Base user Dto</param>
        /// <returns>Returns AuthResponseDto with Bearer Token if logged in successfully | null if unsuccessful</returns>
        public async Task<AuthResponseDto> Login(BaseApiUserDto userDto)
        {
            user = await userManager.FindByEmailAsync(userDto.Email);
            bool isValidUser = await userManager.CheckPasswordAsync(user, userDto.Password);


            if (isValidUser == false || user == null)
            {
                return null;
            }


            var token = await GenerateToken();

            return new AuthResponseDto
            {
                Token = token,
                UserId = user.Id,
                RefreshToken = await CreateRefreshToken()
            };
        }

        /// <summary>
        /// Generates Bearer Token with successful login.
        /// </summary>
        /// <returns>Bearer Token</returns>
        private async Task<string> GenerateToken()
        {
            //Gets defined security key from configurations.
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSettings:Key"]));

            //Transforms security to credentials using Sha256.
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Gets roles from user.
            var roles = await userManager.GetRolesAsync(user);
            //Generates a List of Claims with selected roles.
            var roleClaims = roles.Select(x => new Claim(ClaimTypes.Role, x)).ToList();

            //Gets user claims
            var userClaims = await userManager.GetClaimsAsync(user);

            //Connects claims to be displayed in the Token. 
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("uid", user.Id)
            }.Union(userClaims).Union(roleClaims);

            //Builds the Token including the claims and credentials.
            var token = new JwtSecurityToken(
                issuer: configuration["JwtSettings:Issuer"],
                audience: configuration["JwtSettings:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(Convert.ToInt32(configuration["JwtSettings:DurationInMinutes"])),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Creates a new Refresh Token.
        /// </summary>
        /// <returns>Refresh Token</returns>
        public async Task<string> CreateRefreshToken()
        {
            //Removes Refresh Token from Db Table AspNetUserTokens
            await userManager.RemoveAuthenticationTokenAsync(user, LoginProvider, RefreshToken);
            var newRefreshToken = await userManager.GenerateUserTokenAsync(user, LoginProvider, RefreshToken);
            //Sets new Refresh Token in Db
            await userManager.SetAuthenticationTokenAsync(user, LoginProvider, RefreshToken, newRefreshToken);
            return newRefreshToken;
        }

        /// <summary>
        /// Verifies if given user is applicable for a new Refresh Token.
        /// </summary>
        /// <param name="request">User with Bearer and Refresh Token</param>
        /// <returns>If successful assigns new Bearer and Refresh Token. Save logout if unsuccessful</returns>
        public async Task<AuthResponseDto> VerifyRefreshToken(AuthResponseDto request)
        {
            //Gets user from Db according to Email.
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var tokenContent = jwtSecurityTokenHandler.ReadJwtToken(request.Token);
            var username = tokenContent.Claims.ToList().FirstOrDefault(q => q.Type == JwtRegisteredClaimNames.Email)?.Value;
            user = await userManager.FindByNameAsync(username);

            //Cancels renewal process if specified user was not found.
            //Also compares user Id's. Protects against Email spoofing.
            if (user == null || user.Id != request.UserId)
            {
                return null;
            }

            var isValidRefreshToken = await userManager.VerifyUserTokenAsync(user, LoginProvider, RefreshToken, request.RefreshToken);

            //Generates new Tokens and returns user accordingly.
            if (isValidRefreshToken)
            {
                var token = await GenerateToken();
                return new AuthResponseDto()
                {
                    Token = token,
                    UserId = user.Id,
                    RefreshToken = await CreateRefreshToken()
                };
            }

            //Removes Tokens from user from for save logout.
            await userManager.UpdateSecurityStampAsync(user);
            return null;
        }
    }
}
