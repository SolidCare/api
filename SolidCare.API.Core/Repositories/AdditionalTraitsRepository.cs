﻿using SolidCare.API.Core.Contracts;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Repositories
{
    public class AdditionalTraitsRepository : GenericRepository<AdditionalTraits> , IAdditionalTraitsRepository
    {
        public AdditionalTraitsRepository(ApiDbContext context) : base(context)
        {
        }
    }
}
