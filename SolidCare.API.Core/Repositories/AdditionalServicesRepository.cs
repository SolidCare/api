﻿using SolidCare.API.Core.Contracts;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Repositories
{
    public class AdditionalServicesRepository : GenericRepository<AdditionalServices> , IAdditionalServicesRepository
    {
        public AdditionalServicesRepository(ApiDbContext context) : base(context) {}
    }
}
