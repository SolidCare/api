using Microsoft.EntityFrameworkCore;
using SolidCare.API.Core.Contracts;
using SolidCare.API.Data;
using SolidCare.API.Data.Models;

namespace SolidCare.API.Core.Repositories
{
    public class ChildCareFamilyProfileRepository : GenericRepository<ChildCareFamilyProfile> , IChildCareFamilyProfileRepository
    {
        private readonly ApiDbContext context;

        public ChildCareFamilyProfileRepository(ApiDbContext context) : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// Displays one ChildCareFamilyProfile by Id
        /// Includes all related entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ChildCareFamilyProfile including all related entities</returns>
        public async Task<ChildCareFamilyProfile> GetDetails(int? id)
        {
            return await context.ChildCareFamilyProfile.
                Include(addServ => addServ.AdditionalServices)
                .Include(addTrait => addTrait.AdditionalTraits)
                .Include(avail => avail.Availability)
                .FirstOrDefaultAsync(profile => profile.Id == id);
        }
    }
}