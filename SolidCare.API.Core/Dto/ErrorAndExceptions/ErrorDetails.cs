﻿namespace SolidCare.API.Core.Dto.ErrorAndExceptions
{
    /// <summary>
    /// Error Detail containing
    /// Used for a descriptive response during exception handling.
    /// </summary>
    public class ErrorDetails
    {
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }
    }
}
