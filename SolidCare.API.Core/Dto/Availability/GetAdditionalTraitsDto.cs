﻿namespace SolidCare.API.Core.Dto.Availability
{
    public class GetAvailabilityDto : BaseAvailabilityDto
    {
        public int Id { get; set; }
    }
}
