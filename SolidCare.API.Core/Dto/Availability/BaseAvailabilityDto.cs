﻿using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Core.Dto.Availability
{
    /// <summary>
    /// Contains properties used by all Dto's.
    /// </summary>
    public abstract class BaseAvailabilityDto
    {
        public DateTime AvailableFrom { get; set; }

        public Frequency Frequency { get; set; }

        public int Workinghours { get; set; }

        public WorkinghoursFrequency WorkinghoursFrequency { get; set; }


        //Weeks table
        public bool MondayMorning { get; set; }
        public bool MondayAfternoon { get; set; }
        public bool MondayEvening { get; set; }
        public bool MondayNight { get; set; }
        public bool TuesdayMorning { get; set; }
        public bool TuesdayAfternoon { get; set; }
        public bool TuesdayEvening { get; set; }
        public bool TuesdayNight { get; set; }
        public bool WednesdayMorning { get; set; }
        public bool WednesdayAfternoon { get; set; }
        public bool WednesdayEvening { get; set; }
        public bool WednesdayNight { get; set; }
        public bool ThursdayMorning { get; set; }
        public bool ThursdayAfternoon { get; set; }
        public bool ThursdayEvening { get; set; }
        public bool ThursdayNight { get; set; }
        public bool FridayMorning { get; set; }
        public bool FridayAfternoon { get; set; }
        public bool FridayEvening { get; set; }
        public bool FridayNight { get; set; }
        public bool SaturdayMorning { get; set; }
        public bool SaturdayAfternoon { get; set; }
        public bool SaturdayEvening { get; set; }
        public bool SaturdayNight { get; set; }
        public bool SundayMorning { get; set; }
        public bool SundayAfternoon { get; set; }
        public bool SundayEvening { get; set; }
        public bool SundayNight { get; set; }
    }
}
