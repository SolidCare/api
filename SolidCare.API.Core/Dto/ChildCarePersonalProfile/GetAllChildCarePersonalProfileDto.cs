﻿namespace SolidCare.API.Core.Dto.ChildCarePersonalProfile
{
    public class GetAllChildCarePersonalProfileDto : BaseChildCarePersonalProfileDto
    {
        public int Id { get; set; }
        public int AdditionalServicesId { get; set; }
        public int AdditionalTraitsId { get; set; }
        public int AvailabilityId { get; set; }
    }
}
