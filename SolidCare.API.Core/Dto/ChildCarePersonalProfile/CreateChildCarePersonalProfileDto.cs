﻿using SolidCare.API.Core.Dto.AdditionalServices;
using SolidCare.API.Core.Dto.AdditionalTraits;
using SolidCare.API.Core.Dto.Availability;

namespace SolidCare.API.Core.Dto.ChildCarePersonalProfile
{
    public class CreateChildCarePersonalProfileDto : BaseChildCarePersonalProfileDto
    {
        public CreateAdditionalServicesDto AdditionalServices { get; set; }
        public CreateAdditionalTraitsDto AdditionalTraits { get; set; }
        public CreateAvailabilityDto Availability { get; set; }
    }
}
