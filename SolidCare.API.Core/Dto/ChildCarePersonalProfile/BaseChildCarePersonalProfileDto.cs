﻿using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Core.Dto.ChildCarePersonalProfile
{
    /// <summary>
    /// Contains properties used by all Dto's.
    /// </summary>
    public abstract class BaseChildCarePersonalProfileDto
    {
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Place { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int MaxChildren { get; set; }
        public int Experience { get; set; }

        public CareSubType CareSubType { get; set; }
        public AgeGroup AgeGroup { get; set; }
    }
}
