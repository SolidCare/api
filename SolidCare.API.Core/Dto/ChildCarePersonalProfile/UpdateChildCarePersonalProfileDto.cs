﻿using SolidCare.API.Core.Dto.AdditionalServices;
using SolidCare.API.Core.Dto.AdditionalTraits;
using SolidCare.API.Core.Dto.Availability;

namespace SolidCare.API.Core.Dto.ChildCarePersonalProfile
{
    public class UpdateChildCarePersonalProfileDto : BaseChildCarePersonalProfileDto
    {
        //public int Id { get; set; }
        public CreateAdditionalServicesDto AdditionalServices { get; set; }
        public CreateAdditionalTraitsDto AdditionalTraits { get; set; }
        public CreateAvailabilityDto Availability { get; set; }
    }
}
