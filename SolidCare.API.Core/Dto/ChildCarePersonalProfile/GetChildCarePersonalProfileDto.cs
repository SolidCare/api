﻿namespace SolidCare.API.Core.Dto.ChildCarePersonalProfile
{
    /// <summary>
    /// Dto to display one entity including related objects.
    /// </summary>
    public class GetChildCarePersonalProfileDto : BaseChildCarePersonalProfileDto
    {
        public int Id { get; set; }
        public int AdditionalServicesId { get; set; }
        public int AdditionalTraitsId { get; set; }
        public int AvailabilityId { get; set; }
        public Data.Models.AdditionalServices AdditionalServices { get; set; }
        public Data.Models.AdditionalTraits AdditionalTraits { get; set; }
        public Data.Models.Availability Availability { get; set; }
    }
}
