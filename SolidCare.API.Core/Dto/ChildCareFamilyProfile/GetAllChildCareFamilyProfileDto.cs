﻿namespace SolidCare.API.Core.Dto.ChildCareFamilyProfile
{
    public class GetAllChildCareFamilyProfileDto : BaseChildCareFamilyProfileDto
    {
        public int Id { get; set; }
        public int AdditionalServicesId { get; set; }
        public int AdditionalTraitsId { get; set; }
        public int AvailabilityId { get; set; }
    }
}
