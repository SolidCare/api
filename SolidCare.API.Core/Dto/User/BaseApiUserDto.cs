﻿using System.ComponentModel.DataAnnotations;

namespace SolidCare.API.Core.Dto.User
{
    /// <summary>
    /// Contains properties used by all user Dto's.
    /// </summary>
    public class BaseApiUserDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(256, ErrorMessage = "Password length between {2} and {1} characters."), MinLength(8)]
        public string Password { get; set; }
    }
}
