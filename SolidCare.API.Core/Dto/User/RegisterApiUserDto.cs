﻿using System.ComponentModel.DataAnnotations;
using SolidCare.API.Data.Models.Enums;

namespace SolidCare.API.Core.Dto.User
{
    /// <summary>
    /// User Dto.
    /// Defines registration properties.
    /// </summary>
    public class RegisterApiUserDto : BaseApiUserDto
    {
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public Gender Gender { get; set; }

    }
}
