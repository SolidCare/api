﻿namespace SolidCare.API.Core.Dto.AdditionalServices
{
    public class GetAdditionalServicesDto : BaseAdditionalServicesDto
    {
        public int Id { get; set; }
    }
}
