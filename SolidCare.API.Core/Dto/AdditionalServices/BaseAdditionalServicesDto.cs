﻿namespace SolidCare.API.Core.Dto.AdditionalServices
{
    /// <summary>
    /// Contains properties used by all Dto's.
    /// </summary>
    public abstract class BaseAdditionalServicesDto
    {
        public bool Cook { get; set; }
        public bool HelpWithHomework { get; set; }
        public bool HelpWithHousekeeping { get; set; }
        public bool EscortChild { get; set; }
        public bool MedicationDispensing { get; set; }
    }
}
