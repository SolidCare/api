﻿namespace SolidCare.API.Core.Dto.AdditionalTraits
{    
    /// <summary>
    /// Contains properties used by all Dto's.
    /// </summary>
    public abstract class BaseAdditionalTraitsDto
    {
        public bool Smoker { get; set; }
        public bool AcceptsPets { get; set; }
        public bool CarLicense { get; set; }
        public bool FirstAid { get; set; }
    }
}
