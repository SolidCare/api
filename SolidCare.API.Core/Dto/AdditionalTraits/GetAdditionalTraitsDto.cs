﻿namespace SolidCare.API.Core.Dto.AdditionalTraits
{
    public class GetAdditionalTraitsDto : BaseAdditionalTraitsDto
    {
        public int Id { get; set; }
    }
}
